package application

import (
	"log/slog"
	"net/http"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"

	api "gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/fictief-loket-api"
	"gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/fictief-loket-backend/config"
)

type Application struct {
	*http.Server
	logger *slog.Logger
	cfg    *config.Config

	supportedAttributes map[string]config.Attribute // precomputed map of attribute names with their config
}

func New(logger *slog.Logger, cfg *config.Config) Application {
	app := Application{
		Server: &http.Server{
			Addr: cfg.BackendListenAddress,
		},
		logger: logger,
		cfg:    cfg,
	}
	app.initSupportedAttributes()
	return app
}

func (app *Application) Router() {
	r := chi.NewRouter()

	r.Use(middleware.Logger)
	r.Use(func(next http.Handler) http.Handler {
		fn := func(w http.ResponseWriter, r *http.Request) {
			app.logger.Debug("Request headers", "headers", r.Header)
			next.ServeHTTP(w, r)
		}
		return http.HandlerFunc(fn)
	})
	r.Use(middleware.Recoverer)
	r.Use(middleware.Heartbeat("/healthz"))
	r.Use(middleware.SetHeader("Content-Type", "application/json"))

	apiHandler := api.NewStrictHandler(app, nil)
	r.Mount("/v0", api.Handler(apiHandler))

	r.Group(func(r chi.Router) {
		r.Use(middleware.SetHeader("Content-Type", "application/yaml"))
		r.Get("/openapi.yaml", app.OpenAPI)
	})

	app.Server.Handler = r
}

func (app *Application) initSupportedAttributes() {
	app.supportedAttributes = make(map[string]config.Attribute)
	for _, rubriek := range app.cfg.GlobalConfig.Rubrieken {
		for _, attr := range rubriek.Attributes {
			app.logger.Debug("Added supported attribute", "attr_name", attr.Name)
			app.supportedAttributes[attr.Name] = attr
		}
	}
}
