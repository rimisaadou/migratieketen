package application

import (
	"context"
	"fmt"
	"slices"

	"github.com/google/uuid"
	api "gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/fictief-loket-api"
	"gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/fictief-loket-backend/helpers"
	sigma_api "gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/fictief-sigma-api"
	"golang.org/x/exp/maps"
	"golang.org/x/sync/errgroup"
)

// ReadProcessen implements api.StrictServerInterface.
func (app *Application) ReadProcessen(ctx context.Context, request api.ReadProcessenRequestObject) (api.ReadProcessenResponseObject, error) {
	var sources []string
	if request.Params.Sources == nil || slices.Equal(*request.Params.Sources, api.QuerySources{""}) { // RH: ATTN: note that `?sources=` creates a slice with a single empty string, NOT an empty slice.
		sources = maps.Keys(app.cfg.SigmaFscEndpointNamesBySource)
	} else {
		for _, v := range *request.Params.Sources {
			if _, exists := app.cfg.SigmaFscEndpointNamesBySource[v]; exists {
				sources = append(sources, v)
			} else {
				app.logger.Error("Unknown source SIGMA requested", "src", v, "known_sigmas", maps.Keys(app.cfg.SigmaFscEndpointNamesBySource))
			}
		}
	}

	processes := helpers.ConcurrentSlice[api.Proces]{}

	g, gctx := errgroup.WithContext(ctx)
	for _, source := range sources {

		source := source

		g.Go(func() error {
			client, err := app.getSigmaClient(app.cfg.SigmaFscEndpointNamesBySource[source])
			if err != nil {
				return err
			}

			resp, err := client.ReadProcessenWithResponse(gctx, &sigma_api.ReadProcessenParams{
				Attributes: request.Params.Attributes,
			})
			if err != nil {
				return err
			}

			if resp.JSON200 == nil {
				return fmt.Errorf("failed to call sigma source '%s': %d", source, resp.StatusCode())
			}

			for _, record := range resp.JSON200.Data {
				processes.Append(ToProces(&record))
			}

			return nil
		})
	}

	if err := g.Wait(); err != nil {
		return nil, err
	}

	proc := make([]api.Proces, 0, processes.Length())
	for a := range processes.Iter() {
		proc = append(proc, a)
	}

	return api.ReadProcessen200JSONResponse{
		ProcessenResponseJSONResponse: api.ProcessenResponseJSONResponse{
			Data: proc,
		},
	}, nil

}

// ReadProcesById implements api.StrictServerInterface.
func (app *Application) ReadProcesById(ctx context.Context, request api.ReadProcesByIdRequestObject) (api.ReadProcesByIdResponseObject, error) {
	var sources []string
	if request.Params.Sources == nil || slices.Equal(*request.Params.Sources, api.QuerySources{""}) { // RH: ATTN: note that `?sources=` creates a slice with a single empty string, NOT an empty slice.
		sources = maps.Keys(app.cfg.SigmaFscEndpointNamesBySource)
	} else {
		for _, v := range *request.Params.Sources {
			if _, exists := app.cfg.SigmaFscEndpointNamesBySource[v]; exists {
				sources = append(sources, v)
			} else {
				app.logger.Error("Unknown source SIGMA requested", "src", v, "known_sigmas", maps.Keys(app.cfg.SigmaFscEndpointNamesBySource))
			}
		}
	}

	var proces api.Proces

	g := new(errgroup.Group)

	// Note: because we are expecting one proces we are not taking into account race conditions or anything else since this should not happen.
	for _, source := range sources {

		source := source

		g.Go(func() error {
			client, err := app.getSigmaClient(app.cfg.SigmaFscEndpointNamesBySource[source])
			if err != nil {
				return err
			}

			resp, err := client.ReadProcesByIdWithResponse(ctx, request.ProcesId.String(), &sigma_api.ReadProcesByIdParams{
				Attributes: request.Params.Attributes,
			})
			if err != nil {
				return err
			}

			if resp.JSON200 == nil {
				return fmt.Errorf("failed to call sigma source '%s': %d", source, resp.StatusCode())
			}

			proces = ToProces(&resp.JSON200.Data)

			return nil
		})
	}

	if err := g.Wait(); err != nil && proces.Id == uuid.Nil {
		return nil, err
	}

	return api.ReadProcesById200JSONResponse{
		ProcesResponseJSONResponse: api.ProcesResponseJSONResponse{
			Data: proces,
		},
	}, nil
}

// ReadProcessenByVreemdelingId implements api.StrictServerInterface.
func (app *Application) ReadProcessenByVreemdelingId(ctx context.Context, request api.ReadProcessenByVreemdelingIdRequestObject) (api.ReadProcessenByVreemdelingIdResponseObject, error) {
	var sources []string
	if request.Params.Sources == nil || slices.Equal(*request.Params.Sources, api.QuerySources{""}) { // RH: ATTN: note that `?sources=` creates a slice with a single empty string, NOT an empty slice.
		sources = maps.Keys(app.cfg.SigmaFscEndpointNamesBySource)
	} else {
		for _, v := range *request.Params.Sources {
			if _, exists := app.cfg.SigmaFscEndpointNamesBySource[v]; exists {
				sources = append(sources, v)
			} else {
				app.logger.Error("Unknown source SIGMA requested", "src", v, "known_sigmas", maps.Keys(app.cfg.SigmaFscEndpointNamesBySource))
			}
		}
	}

	processes := helpers.ConcurrentSlice[api.Proces]{}

	g, gctx := errgroup.WithContext(ctx)
	for _, source := range sources {

		source := source

		g.Go(func() error {
			client, err := app.getSigmaClient(app.cfg.SigmaFscEndpointNamesBySource[source])
			if err != nil {
				return err
			}

			resp, err := client.ReadProcessenByVreemdelingIdWithResponse(gctx, request.VreemdelingId, &sigma_api.ReadProcessenByVreemdelingIdParams{
				Attributes: request.Params.Attributes,
			})
			if err != nil {
				return err
			}

			if resp.JSON200 == nil {
				return fmt.Errorf("failed to call sigma source '%s': %d", source, resp.StatusCode())
			}

			for _, record := range resp.JSON200.Data {
				processes.Append(ToProces(&record))
			}

			return nil
		})
	}

	if err := g.Wait(); err != nil {
		return nil, err
	}

	proc := make([]api.Proces, 0, processes.Length())
	for a := range processes.Iter() {
		proc = append(proc, a)
	}

	return api.ReadProcessenByVreemdelingId200JSONResponse{
		ProcessenResponseJSONResponse: api.ProcessenResponseJSONResponse{
			Data: proc,
		},
	}, nil
}

func ToProces(record *sigma_api.Proces) api.Proces {
	proces := api.Proces{
		CreatedAt:     record.CreatedAt,
		DeletedAt:     record.DeletedAt,
		Id:            record.Id,
		ProcesId:      record.ProcesId,
		VreemdelingId: record.VreemdelingId,
		Status:        record.Status,
		Type:          record.Type,
		Source:        record.Source,
	}

	if record.Attributes != nil {
		attributes := map[string]api.Attribute{}

		for k, attr := range *record.Attributes {
			procAttr := attributes[k]
			procAttr.Value = attr.Value

			for _, a := range attr.Observations {
				procAttr.Observations = append(procAttr.Observations, (api.Observation)(a))
			}

			attributes[k] = procAttr
		}

		proces.Attributes = &attributes
	}

	return proces
}
