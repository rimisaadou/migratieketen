package application

import (
	"testing"
	"time"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
	api "gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/fictief-loket-api"
)

func TestApplication_mergeObservations(t *testing.T) {
	type args struct {
		attributes       map[string]bool
		obssByAttrAndSrc map[string]map[string][]bareObservationValue
	}
	tests := []struct {
		name string
		args args
		want map[string]api.Attribute
	}{
		{
			name: "nil attributes, nil observations",
			args: args{
				nil,
				nil,
			},
			want: map[string]api.Attribute{},
		},
		{
			name: "no attributes, no observations",
			args: args{
				map[string]bool{},
				map[string]map[string][]bareObservationValue{},
			},
			want: map[string]api.Attribute{},
		},
		{
			name: "single attribute, no observations",
			args: args{
				map[string]bool{
					"attr1": true,
				},
				map[string]map[string][]bareObservationValue{},
			},
			want: map[string]api.Attribute{},
		},
		{
			name: "multiple attributes, no observations",
			args: args{
				map[string]bool{
					"attr2": true,
					"attr1": true,
					"attr3": true,
				},
				map[string]map[string][]bareObservationValue{},
			},
			want: map[string]api.Attribute{},
		},
		{
			name: "no attributes, single observation",
			args: args{
				map[string]bool{},
				map[string]map[string][]bareObservationValue{
					"attr1": {
						"src1": {
							{
								Id:            uuid.MustParse("00000000-0000-0000-0000-000000000001"),
								Value:         "foo",
								CreatedAt:     time.Date(2023, 9, 15, 13, 37, 0, 0, time.UTC),
								InvalidatedAt: nil,
							},
						},
					},
				},
			},
			want: map[string]api.Attribute{},
		},
		{
			name: "no attributes, multiple observation",
			args: args{
				map[string]bool{},
				map[string]map[string][]bareObservationValue{
					"attr1": {
						"src1": {
							{
								Id:            uuid.MustParse("00000001-0001-0000-0000-000000000002"),
								Value:         "foo",
								CreatedAt:     time.Date(2023, 9, 15, 13, 37, 1, 0, time.UTC),
								InvalidatedAt: nil,
							},
							{
								Id:            uuid.MustParse("00000001-0001-0000-0000-000000000003"),
								Value:         "foo",
								CreatedAt:     time.Date(2023, 9, 15, 13, 37, 2, 0, time.UTC),
								InvalidatedAt: nil,
							},
							{
								Id:            uuid.MustParse("00000001-0001-0000-0000-000000000001"),
								Value:         "foo",
								CreatedAt:     time.Date(2023, 9, 15, 13, 36, 59, 0, time.UTC),
								InvalidatedAt: nil,
							},
						},
						"src2": {
							{
								Id:            uuid.MustParse("00000002-0001-0000-0000-000000000001"),
								Value:         "foo",
								CreatedAt:     time.Date(2023, 9, 15, 13, 37, 0, 0, time.UTC),
								InvalidatedAt: nil,
							},
						},
					},
					"attr2": {
						"src1": {
							{
								Id:            uuid.MustParse("00000001-0002-0000-0000-000000000001"),
								Value:         "foo",
								CreatedAt:     time.Date(2023, 9, 15, 13, 37, 0, 0, time.UTC),
								InvalidatedAt: nil,
							},
						},
					},
				},
			},
			want: map[string]api.Attribute{},
		},
		{
			name: "single attribute, multiple observation",
			args: args{
				map[string]bool{
					"attr1": true,
				},
				map[string]map[string][]bareObservationValue{
					"attr1": {
						"src1": {
							{
								Id:            uuid.MustParse("00000001-0001-0000-0000-000000000002"),
								Value:         "foo",
								CreatedAt:     time.Date(2023, 9, 15, 13, 37, 0, 0, time.UTC),
								InvalidatedAt: nil,
							},
						},
						"src2": {
							{
								Id:            uuid.MustParse("00000002-0001-0000-0000-000000000001"),
								Value:         "bar",
								CreatedAt:     time.Date(2023, 9, 15, 13, 37, 1, 0, time.UTC),
								InvalidatedAt: nil,
							},
						},
					},
				},
			},
			want: map[string]api.Attribute{
				"attr1": {
					Observations: []api.Observation{
						{
							Id:            uuid.MustParse("00000002-0001-0000-0000-000000000001"),
							Value:         "bar",
							CreatedAt:     time.Date(2023, 9, 15, 13, 37, 1, 0, time.UTC),
							InvalidatedAt: nil,
							Attribute:     "attr1",
							Source:        "src2",
						},
						{
							Id:            uuid.MustParse("00000001-0001-0000-0000-000000000002"),
							Value:         "foo",
							CreatedAt:     time.Date(2023, 9, 15, 13, 37, 0, 0, time.UTC),
							InvalidatedAt: nil,
							Attribute:     "attr1",
							Source:        "src1",
						},
					},
					Value: "bar",
				},
			},
		},
		{
			name: "single attribute, unsorted observations",
			args: args{
				map[string]bool{
					"attr1": true,
				},
				map[string]map[string][]bareObservationValue{
					"attr1": {
						"src1": {
							{
								Id:            uuid.MustParse("00000001-0001-0000-0000-000000000002"),
								Value:         "foo",
								CreatedAt:     time.Date(2023, 9, 15, 13, 37, 1, 0, time.UTC),
								InvalidatedAt: nil,
							},
							{
								Id:            uuid.MustParse("00000001-0001-0000-0000-000000000003"),
								Value:         "bar",
								CreatedAt:     time.Date(2023, 9, 15, 13, 37, 2, 0, time.UTC),
								InvalidatedAt: nil,
							},
							{
								Id:            uuid.MustParse("00000001-0001-0000-0000-000000000001"),
								Value:         "foo",
								CreatedAt:     time.Date(2023, 9, 15, 13, 36, 59, 0, time.UTC),
								InvalidatedAt: nil,
							},
						},
						"src2": {
							{
								Id:            uuid.MustParse("00000002-0001-0000-0000-000000000001"),
								Value:         "foo",
								CreatedAt:     time.Date(2023, 9, 15, 13, 37, 0, 0, time.UTC),
								InvalidatedAt: nil,
							},
						},
					},
				},
			},
			want: map[string]api.Attribute{
				"attr1": {
					Observations: []api.Observation{
						{
							Id:            uuid.MustParse("00000001-0001-0000-0000-000000000003"),
							Value:         "bar",
							CreatedAt:     time.Date(2023, 9, 15, 13, 37, 2, 0, time.UTC),
							InvalidatedAt: nil,
							Attribute:     "attr1",
							Source:        "src1",
						},
						{
							Id:            uuid.MustParse("00000001-0001-0000-0000-000000000002"),
							Value:         "foo",
							CreatedAt:     time.Date(2023, 9, 15, 13, 37, 1, 0, time.UTC),
							InvalidatedAt: nil,
							Attribute:     "attr1",
							Source:        "src1",
						},
						{
							Id:            uuid.MustParse("00000002-0001-0000-0000-000000000001"),
							Value:         "foo",
							CreatedAt:     time.Date(2023, 9, 15, 13, 37, 0, 0, time.UTC),
							InvalidatedAt: nil,
							Attribute:     "attr1",
							Source:        "src2",
						},
						{
							Id:            uuid.MustParse("00000001-0001-0000-0000-000000000001"),
							Value:         "foo",
							CreatedAt:     time.Date(2023, 9, 15, 13, 36, 59, 0, time.UTC),
							InvalidatedAt: nil,
							Attribute:     "attr1",
							Source:        "src1",
						},
					},
					Value: "bar",
				},
			},
		},
		{
			name: "mismatch between attributes and observations",
			args: args{
				map[string]bool{
					"attr1": true,
					"attr2": true,
				},
				map[string]map[string][]bareObservationValue{
					"attr2": {
						"src1": {
							{
								Id:            uuid.MustParse("00000001-0002-0000-0000-000000000001"),
								Value:         []string{"baz"},
								CreatedAt:     time.Date(2023, 9, 15, 13, 37, 0, 0, time.UTC),
								InvalidatedAt: nil,
							},
						},
					},
					"attr3": {
						"src1": {
							{
								Id:            uuid.MustParse("00000001-0001-0000-0000-000000000002"),
								Value:         "foo",
								CreatedAt:     time.Date(2023, 9, 15, 13, 37, 1, 0, time.UTC),
								InvalidatedAt: nil,
							},
						},
					},
				},
			},
			want: map[string]api.Attribute{
				"attr2": {
					Observations: []api.Observation{
						{
							Id:            uuid.MustParse("00000001-0002-0000-0000-000000000001"),
							Value:         []string{"baz"},
							CreatedAt:     time.Date(2023, 9, 15, 13, 37, 0, 0, time.UTC),
							InvalidatedAt: nil,
							Attribute:     "attr2",
							Source:        "src1",
						},
					},
					Value: []string{"baz"},
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			app := &Application{}
			assert.Equal(t, tt.want, app.mergeObservations(tt.args.attributes, tt.args.obssByAttrAndSrc))
		})
	}
}
