package application

import (
	"context"
	"fmt"
	"net/http"
	"slices"
	"sort"
	"time"

	"github.com/google/uuid"
	"golang.org/x/exp/maps"
	"golang.org/x/sync/errgroup"

	api "gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/fictief-loket-api"
	sigma_api "gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/fictief-sigma-api"
)

// contains just the value and metadata of an observation, to be used in a situation where attribute and source are known.
type bareObservationValue struct {
	Id            uuid.UUID
	Value         any
	CreatedAt     time.Time
	InvalidatedAt *time.Time
}

// Implements api.StrictServerInterface.
func (app *Application) ReadVreemdelingById(ctx context.Context, request api.ReadVreemdelingByIdRequestObject) (api.ReadVreemdelingByIdResponseObject, error) {
	// Verify and parse request:
	vnr := request.VreemdelingId
	attributes := map[string]bool{} // Set of requested attributes
	for _, f := range request.Params.Attributes {
		if _, exists := app.supportedAttributes[f]; !exists {
			return api.ReadVreemdelingById400JSONResponse{
				BadRequestErrorResponseJSONResponse: api.BadRequestErrorResponseJSONResponse{
					Errors: []api.Error{{Message: fmt.Sprintf("Attribute '%s' not supported in attributes parameter", f)}},
				},
			}, nil
		}
		attributes[f] = true
	}
	var sources []string
	if request.Params.Sources == nil || slices.Equal(*request.Params.Sources, api.QuerySources{""}) { // RH: ATTN: note that `?sources=` creates a slice with a single empty string, NOT an empty slice.
		sources = maps.Keys(app.cfg.SigmaFscEndpointNamesBySource)
	} else {
		for _, v := range *request.Params.Sources {
			if _, exists := app.cfg.SigmaFscEndpointNamesBySource[v]; exists {
				sources = append(sources, v)
			} else {
				app.logger.Error("Unknown source SIGMA requested", "src", v, "known_sigmas", maps.Keys(app.cfg.SigmaFscEndpointNamesBySource))
			}
		}
	}

	bvv, err := app.getBvvClient()
	if err != nil {
		panic(err)
	}
	// RH: TODO: maybe these 2 BVV calls should be one?
	// ie. a call to just get all alternative vnrs, regardless if any merges have been performed?
	if vrResp, err := bvv.ReadVreemdelingByIdWithResponse(ctx, vnr); err != nil {
		panic(err)
	} else {
		if vrResp.StatusCode() == http.StatusGone {
			return api.ReadVreemdelingById410JSONResponse{
				GoneErrorResponseJSONResponse: api.GoneErrorResponseJSONResponse{
					Errors: []api.Error{{Message: "VreemdelingID not found"}},
				},
			}, nil
		}
		if vrResp.JSON200 == nil {
			panic("failed to call BVV")
		}
	}
	vnrSet := map[string]struct{}{
		vnr: {},
	}
	if vrResp, err := bvv.GetSamenvoegingenWithResponse(ctx, vnr); err != nil {
		panic(err)
	} else {
		if vrResp.JSON200 == nil {
			panic("failed to call BVV")
		}
		for _, v := range vrResp.JSON200.Data {
			vnrSet[v] = struct{}{}
		}
	}

	obssByAttrAndSrc := app.readObservationsFromAllSigmaSources(ctx, maps.Keys(vnrSet), attributes, sources)
	vreemdeling := &api.VreemdelingWithObservations{
		Id:                   vnr,
		AdditionalProperties: app.mergeObservations(attributes, obssByAttrAndSrc),
	}
	return api.ReadVreemdelingById200JSONResponse{
		VreemdelingWithObservationsResponseJSONResponse: api.VreemdelingWithObservationsResponseJSONResponse{
			Data: *vreemdeling,
		},
	}, nil
}

// Merge attribute observations from different sources.
// RH: ATTN: this can't be completely generic, because merge logic might be different for different attributes.
func (app *Application) mergeObservations(attributes map[string]bool, obssByAttrAndSrc map[string]map[string][]bareObservationValue) map[string]api.Attribute {
	result := make(map[string]api.Attribute, len(attributes))
	for attr := range attributes {
		var lastObsCreatedAt time.Time
		var lastObsOrValue bool
		var avgCount int
		var avgTotal int
		var avgBool bool
		var minIsSet bool
		var maxBool bool
		var maxValue int
		var minValue int
		var concatString string
		if obssBySrc, exists := obssByAttrAndSrc[attr]; exists {
			attrWithObs := api.Attribute{}
			for src, obss := range obssBySrc {
				for _, obs := range obss {
					attrWithObs.Observations = append(attrWithObs.Observations, api.Observation{
						Id:            obs.Id,
						Attribute:     attr,
						Value:         obs.Value,
						CreatedAt:     obs.CreatedAt,
						InvalidatedAt: obs.InvalidatedAt,
						Source:        src,
					})

					if app.cfg == nil {
						// RH: TODO: merge logic. Currently just takes the last one (by CreatedAt time).
						if obs.CreatedAt.After(lastObsCreatedAt) {
							lastObsCreatedAt = obs.CreatedAt
							attrWithObs.Value = obs.Value
						}
					} else {

						for _, rubriek := range app.cfg.Rubrieken {
							for _, attrib := range rubriek.Attributes {
								if attr == attrib.Name {
									if attrib.Aggregator == "or" && attrib.Type == "boolean" {
										app.logger.Error("Attr", "attr", attrib.Name)
										if lastObsOrValue == false && obs.Value == true {
											lastObsOrValue = true
											attrWithObs.Value = obs.Value
										} else if lastObsOrValue == false && obs.Value == false {
											attrWithObs.Value = obs.Value
										}

									} else if attrib.Aggregator == "avg" && attrib.Type == "number" {
										avgBool = true
										avgCount += 1
										if v, ok := obs.Value.(int); ok {
											avgTotal += v
										}

									} else if attrib.Aggregator == "max" && attrib.Type == "number" {
										maxBool = true
										if v, ok := obs.Value.(int); ok {
											if v > maxValue {
												maxValue = v
											}
										}

									} else if attrib.Aggregator == "min" && attrib.Type == "number" {
										if v, ok := obs.Value.(int); ok {
											if minIsSet == false {
												minIsSet = true
												minValue = v
											} else if v < minValue {
												minValue = v
											}
										}
									} else if attrib.Aggregator == "concat" && attrib.Type == "text" {
										if v, ok := obs.Value.(string); ok {
											if concatString == "" {
												concatString = v
											} else {
												concatString += ", " + v
											}
										}

									} else {
										// RH: TODO: merge logic. Currently just takes the last one (by CreatedAt time).
										if obs.CreatedAt.After(lastObsCreatedAt) {
											lastObsCreatedAt = obs.CreatedAt
											attrWithObs.Value = obs.Value
										}
									}
								}
							}
						}
					}
				}
			}

			if avgBool == true {
				attrWithObs.Value = avgTotal / avgCount
			} else if maxBool == true {
				attrWithObs.Value = maxValue
			} else if minIsSet == true {
				attrWithObs.Value = minValue
			} else if concatString != "" {
				attrWithObs.Value = concatString
			}

			// RH: TODO: this might be slow when there are many sources with many observations, sorting being O(n log n).
			// If that's a problem, we might change the internal datastructure to use something like a sorted btree for the observations, they're automatically sorted on insert.
			sort.SliceStable(attrWithObs.Observations, func(i, j int) bool {
				return attrWithObs.Observations[i].CreatedAt.After(attrWithObs.Observations[j].CreatedAt)
			})
			result[attr] = attrWithObs
		}
	}
	return result
}

// Returns a map of bare observations (ie. without source and attribute name) keyed by attribute name and source.
func (app *Application) readObservationsFromAllSigmaSources(ctx context.Context, vnrs []string, attributes map[string]bool, sources []string) map[string]map[string][]bareObservationValue {
	obssByAttrAndSrc := map[string]map[string][]bareObservationValue{}
	obsChan := make(chan sigma_api.Observation) // observations sent to this channel are added to the map of observations
	g := errgroup.Group{}
	for _, vnr := range vnrs {
		vnr := vnr
		for _, src := range sources {
			src := src
			g.Go(func() error {
				sigma, err := app.getSigmaClient(app.cfg.SigmaFscEndpointNamesBySource[src])
				if err != nil {
					app.logger.Error("Failed to create SIGMA client", "src", src, "err", err)
					return err
				}
				vrResp, err := sigma.ReadVreemdelingByIdWithResponse(ctx, vnr, &sigma_api.ReadVreemdelingByIdParams{
					Attributes: maps.Keys(attributes),
				})
				if err != nil {
					app.logger.Error("Request to SIGMA failed", "src", src, "err", err)
					return err
				}
				if vrResp.JSON200 == nil {
					err := fmt.Errorf("unexpected status code: %d", vrResp.StatusCode())
					app.logger.Error("Request to SIGMA returned error", "src", src, "err", err, "status_code", vrResp.StatusCode())
					return err
				}
				for attr := range attributes {
					if vrAttr, exists := vrResp.JSON200.Data[attr]; exists {
						for _, obs := range vrAttr.Observations {
							obsChan <- obs
						}
					}
				}
				return nil
			})
		}
	}

	go func() {
		if err := g.Wait(); err != nil {
			app.logger.Error("Error waiting for all SIGMA queries", "err", err)
		}
		close(obsChan)
	}()

	for obs := range obsChan {
		attr := obs.Attribute
		src := obs.Source
		bareObs := bareObservationValue{
			Id:            obs.Id,
			Value:         obs.Value,
			CreatedAt:     obs.CreatedAt,
			InvalidatedAt: obs.InvalidatedAt,
		}
		if _, exists := obssByAttrAndSrc[attr]; !exists {
			obssByAttrAndSrc[attr] = map[string][]bareObservationValue{}
		}
		obssByAttrAndSrc[attr][src] = append(obssByAttrAndSrc[attr][src], bareObs)
	}
	return obssByAttrAndSrc
}
