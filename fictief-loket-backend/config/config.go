package config

import (
	"fmt"
	"log/slog"
	"os"
	"regexp"

	"github.com/iamolegga/enviper"
	"github.com/spf13/viper"
	"gopkg.in/yaml.v3"
)

type Attribute struct {
	Name       string      `yaml:"name"`
	Label      string      `yaml:"label"`
	Type       string      `yaml:"type"`
	Aggregator string      `yaml:"aggregator"`
	Multiple   bool        `yaml:"multiple"` // For types 'select' and 'file'
	Default    interface{} `yaml:"default"`

	// For number types
	Min  *interface{} `yaml:"min"`
	Max  *interface{} `yaml:"max"`
	Step *interface{} `yaml:"step"`

	// For select, radios, and checkboxes types
	Options []struct {
		Name  string `yaml:"name"`
		Label string `yaml:"label"`
	} `yaml:"options"`
}

type Rubriek struct {
	Name  string `yaml:"name"`
	Label string `yaml:"label"`

	Attributes []Attribute `yaml:"attributes"`
}

type GlobalConfig struct {
	Rubrieken []Rubriek `yaml:"rubrieken"`
}

type FscConfig map[string]struct {
	Endpoint     string `yaml:"endpoint"`
	FscGrantHash string `yaml:"FscGrantHash"`
}

type Config struct {
	GlobalConfig `mapstructure:",squash"`
	FscConfig
	Debug                         bool
	BackendListenAddress          string
	SigmaFscEndpointNamesBySource map[string]string
}

// New composes a config with values from the specified paths
func New(globalConfigPath string, configPath string, fscConfigPath string) (*Config, error) {
	e := enviper.New(viper.New())
	e.AllowEmptyEnv(true)
	e.AutomaticEnv()
	e.SetEnvPrefix("MK")
	e.SetDefault("BACKENDLISTENADDRESS", ":8080")

	config := new(Config)
	if err := e.Unmarshal(config); err != nil {
		panic("unable to decode into config struct")
	}

	if err := readConfig(globalConfigPath, &config.GlobalConfig); err != nil {
		return nil, err
	}
	if err := readConfig(fscConfigPath, &config.FscConfig); err != nil {
		return nil, err
	}

	config.SigmaFscEndpointNamesBySource = getSigmaFscEndpointNamesBySourceFromFscConfig(config.FscConfig)

	return config, nil
}

func readConfig(path string, cfg any) error {
	file, err := os.ReadFile(path)
	if err != nil {
		return fmt.Errorf("error reading config file: %w", err)
	}

	if err := yaml.Unmarshal(file, cfg); err != nil {
		return fmt.Errorf("unmarshalling config failed: %w", err)
	}

	return nil
}

// RH: TODO: ATTN: poor man's service discovery, this is extremely brittle.
var sigmaBackendNameRe = regexp.MustCompile(`^(.*)+-sigma-backend$`)
var fscGrantHashRe = regexp.MustCompile(`^\$[a-z0-1,=]+\$`) // https://en.wikipedia.org/wiki/Crypt_(C)#Key_derivation_functions_supported_by_crypt

func getSigmaFscEndpointNamesBySourceFromFscConfig(cfg FscConfig) map[string]string {
	result := map[string]string{}
	for k, v := range cfg {
		if m := sigmaBackendNameRe.FindStringSubmatch(k); m != nil {
			if fscGrantHashRe.MatchString(v.FscGrantHash) {
				slog.Info("Added SIGMA endpoint as source", "name", k)
				result[m[1]] = k
			} else {
				slog.Error("Invalid FSC grant hash for SIGMA endpoint", "name", k, "hash", v.FscGrantHash)
			}
		}
	}

	return result
}
