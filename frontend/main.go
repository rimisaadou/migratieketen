package main

import (
	"log"
	"os"
	"strings"

	"github.com/spf13/cobra"
	"gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/frontend/cmd"
)

const envPrefix = "MK_"

func main() {
	// All flags can also be set using environment variables that start with the specified prefix
	// Environment variable names are all caps and use '_' instead of '-'
	// Example: the value of environment variable 'MK_CONFIG_PATH' is passed to flag 'config-path'
	setupFlagsForEnvironment(cmd.RootCmd)

	err := cmd.RootCmd.Execute()
	if err != nil {
		log.Fatalln("Error running command", err)
	}
}

func setupFlagsForEnvironment(rootCmd *cobra.Command) {
	// Get all environment variables that start with the prefix
	lenPrefix := len(envPrefix)

	for _, keyval := range os.Environ() {
		if strings.HasPrefix(keyval, envPrefix) {
			components := strings.SplitN(keyval[lenPrefix:], "=", 2) //nolint:gomnd // key value pair has to have two components
			if len(components) != 2 {                                //nolint:gomnd // key value pair has to have two components
				continue
			}

			key := strings.ReplaceAll(strings.ToLower(components[0]), "_", "-")
			value := components[1]

			setupCmdFlagForEnvironment(key, value, rootCmd)
		}
	}
}

func setupCmdFlagForEnvironment(key, value string, command *cobra.Command) {
	if flag := command.Flags().Lookup(key); flag != nil {
		if err := flag.Value.Set(value); err != nil {
			log.Fatal(err)
		}

		// Remove required flag if the value was set by an environment variable
		if value != "" {
			delete(flag.Annotations, cobra.BashCompOneRequiredFlag)
		}
	}

	for _, subCmd := range command.Commands() {
		setupCmdFlagForEnvironment(key, value, subCmd)
	}
}
