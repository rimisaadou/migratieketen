package nl.ind.veroordeling.websocket;

import com.fasterxml.jackson.core.type.TypeReference;
import jakarta.websocket.*;

import java.io.IOException;
import java.net.URI;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.client.RestTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.fasterxml.jackson.databind.ObjectMapper;


@ClientEndpoint
public class WebSocketClient {

    private static final Logger logger = LoggerFactory.getLogger(WebSocketClient.class);

//    @Value("${vo.api-url}")
//    private String api_url;

    @Value("${vo.type-terugkeer}")
    private String type_terugkeer;

    @Value("${vo.status-nementerugkeerbesluit}")
    private String status_nementerugkeerbesluit;
    private Session session;
    private final RestTemplate restTemplate;
    private static final String KEY_TYPE = "type";
    private static final String KEY_STATUS = "status";
    private static final String KEY_ATTRIBUTES = "attributes";
    private static final String KEY_ATTRIBUTE = "attribute";
    private static final String KEY_VALUE = "value";
    private static final String KEY_DATA = "data";
    private static final String ATTRIBUTE_SPREEKTALEN = "spreektalen";
    private static final List<String> VALUES_LANGUAGES = Arrays.asList("en", "nl");


    public WebSocketClient(URI endpointURI, RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
        try {
            WebSocketContainer container = ContainerProvider.getWebSocketContainer();
            container.connectToServer(this, endpointURI);
        } catch (Exception e) {
            logger.error("WebSocket connection error", e);
            throw new RuntimeException(e);
        }
    }

    @OnOpen
    public void onOpen(Session session) {
        this.session = session;
        logger.info("Connected to WebSocket server");
    }

    @OnMessage
    public void onMessage(String message) {

        logger.info("Received message: {}", message);

        ObjectMapper objectMapper = new ObjectMapper();
        try {
            Map<String, Object> messageMap = objectMapper.readValue(message, new TypeReference<Map<String, Object>>() {});
            Map<String, Object> dataMap = (Map<String, Object>) messageMap.get("data");
            String vreemdelingId = dataMap != null ? (String) dataMap.get("vreemdelingId") : null;

            logger.info("###1  vreemdlingId: {}", vreemdelingId);
            Map<String, Object> payload = createJsonPayload();

            logger.info("###2 Api Payload: {}", objectMapper.writeValueAsString(payload));

            callRestApi(payload, vreemdelingId);
        } catch (IOException e) {
            logger.error("Error parsing JSON message", e);
        }

      }

    private void callRestApi(Map<String, Object> jsonData, String vreemdelingId) {

        String api_url = "http://ind-sigma-backend-svc/v0/vreemdelingen/" + vreemdelingId + "/processen";
        logger.info("###3 apiUrl: {}", api_url);

        try {
            String response = restTemplate.postForObject(api_url , jsonData, String.class);
            logger.info("Response from REST API: {}", response);
        } catch (Exception e) {
            logger.error("Error calling REST API", e);
        }
    }


    @OnClose
    public void onClose(Session session, CloseReason closeReason) {
        logger.info("WebSocket closed: {}", closeReason);
    }

    @OnError
    public void onError(Throwable throwable) {
        logger.error("WebSocket error", throwable);
    }

    public void sendMessage(String message) {
        this.session.getAsyncRemote().sendText(message);
    }

    private Map<String, Object> createJsonPayload() {
        Map<String, Object> attribute = new HashMap<>();
        attribute.put(KEY_ATTRIBUTE, ATTRIBUTE_SPREEKTALEN);
        attribute.put(KEY_VALUE, VALUES_LANGUAGES);

        Map<String, Object> data = new HashMap<>();
        data.put(KEY_TYPE, type_terugkeer);
        data.put(KEY_STATUS, status_nementerugkeerbesluit);
        data.put(KEY_ATTRIBUTES, Arrays.asList(attribute));

        Map<String, Object> payload = new HashMap<>();
        payload.put(KEY_DATA, data);

        return payload;
    }
}
