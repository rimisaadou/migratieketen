package application

import (
	"context"
	"encoding/json"
	"fmt"
	"time"

	"github.com/google/uuid"
	api "gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/fictief-sigma-api"
	queries "gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/fictief-sigma-backend/application/storage/queries/generated"
)

// ReadProcesById implements api.StrictServerInterface.
func (app *Application) ReadProcesById(ctx context.Context, request api.ReadProcesByIdRequestObject) (api.ReadProcesByIdResponseObject, error) {
	process, err := app.readProcesById(ctx, request.ProcesId, request.Params.Attributes)
	if err != nil {
		return nil, err
	}

	vwlID := uuid.New()
	if request.Params.FSCVWLVerwerkingsSpan != nil {
		vwlID = *request.Params.FSCVWLVerwerkingsSpan
	}

	return api.ReadProcesById200JSONResponse{
		ProcesResponseJSONResponse: api.ProcesResponseJSONResponse{
			Body: api.ProcesResponse{
				Data: *process,
			},
			Headers: api.ProcesResponseResponseHeaders{
				FSCVWLSysteem:               app.vwlSystem,
				FSCVWLVerantwoordelijke:     app.cfg.Organization.Name,
				FSCVWLVerwerker:             app.cfg.Organization.Name,
				FSCVWLVerwerkingsActiviteit: "https://example.com/activity/v0/proces-read",
				FSCVWLVerwerkingsSpan:       vwlID,
			},
		},
	}, nil
}

// ReadProcessen implements api.StrictServerInterface.
func (app *Application) ReadProcessen(ctx context.Context, request api.ReadProcessenRequestObject) (api.ReadProcessenResponseObject, error) {
	records, err := app.db.Queries.ProcessesGet(ctx)
	if err != nil {
		return nil, fmt.Errorf("processes get failed: %w", err)
	}

	processes := make([]api.Proces, 0, len(records))
	for idx := range records {
		process := ToProcess(records[idx], app.source)

		if request.Params.Attributes != nil {
			params := &queries.AttributeListParams{
				ProcessID: records[idx].ID,
				Column2:   *request.Params.Attributes,
			}

			attributeRecords, err := app.db.Queries.AttributeList(ctx, params)
			if err != nil {
				return nil, err
			}

			process.Attributes = ToAttributes(attributeRecords, app.source)
		}

		processes = append(processes, *process)
	}

	vwlID := uuid.New()
	if request.Params.FSCVWLVerwerkingsSpan != nil {
		vwlID = *request.Params.FSCVWLVerwerkingsSpan
	}

	return api.ReadProcessen200JSONResponse{
		ProcessenResponseJSONResponse: api.ProcessenResponseJSONResponse{
			Body: api.ProcessenResponse{
				Data: processes,
			},
			Headers: api.ProcessenResponseResponseHeaders{
				FSCVWLSysteem:               app.vwlSystem,
				FSCVWLVerantwoordelijke:     app.cfg.Organization.Name,
				FSCVWLVerwerker:             app.cfg.Organization.Name,
				FSCVWLVerwerkingsActiviteit: "https://example.com/activity/v0/processen-read",
				FSCVWLVerwerkingsSpan:       vwlID,
			},
		},
	}, err
}

// ReadProcessenByVreemdelingId implements api.StrictServerInterface.
func (app *Application) ReadProcessenByVreemdelingId(ctx context.Context, request api.ReadProcessenByVreemdelingIdRequestObject) (api.ReadProcessenByVreemdelingIdResponseObject, error) {
	records, err := app.db.Queries.ProcessesGetVreemdelingId(ctx, request.VreemdelingId)
	if err != nil {
		return nil, err
	}

	processes := make([]api.Proces, 0, len(records))
	for idx := range records {
		process := ToProcess(records[idx], app.source)

		if request.Params.Attributes != nil {
			params := &queries.AttributeListParams{
				ProcessID: records[idx].ID,
				Column2:   *request.Params.Attributes,
			}

			attributeRecords, err := app.db.Queries.AttributeList(ctx, params)
			if err != nil {
				return nil, err
			}

			process.Attributes = ToAttributes(attributeRecords, app.source)
		}

		processes = append(processes, *process)
	}

	vwlID := uuid.New()
	if request.Params.FSCVWLVerwerkingsSpan != nil {
		vwlID = *request.Params.FSCVWLVerwerkingsSpan
	}

	return api.ReadProcessenByVreemdelingId200JSONResponse{
		ProcessenResponseJSONResponse: api.ProcessenResponseJSONResponse{
			Body: api.ProcessenResponse{
				Data: processes,
			},
			Headers: api.ProcessenResponseResponseHeaders{
				FSCVWLSysteem:               app.vwlSystem,
				FSCVWLVerantwoordelijke:     app.cfg.Organization.Name,
				FSCVWLVerwerker:             app.cfg.Organization.Name,
				FSCVWLVerwerkingsActiviteit: "https://example.com/activity/v0/processen-read",
				FSCVWLVerwerkingsSpan:       vwlID,
			},
		},
	}, nil
}

// CreateAttributeByProcesId implements api.StrictServerInterface.
func (app *Application) CreateAttributeByProcesId(ctx context.Context, request api.CreateAttributeByProcesIdRequestObject) (api.CreateAttributeByProcesIdResponseObject, error) {
	process, err := app.readProcesById(ctx, request.ProcesId, nil)
	if err != nil {
		return nil, fmt.Errorf("process get failed: %w", err)
	}

	if err := validateAttribute(request.Body.Data, app.processes[process.Type]); err != nil {
		errors := fmt.Sprintf("validation failed: %s", err.Error())

		return api.CreateAttributeByProcesId400JSONResponse{
			BadRequestErrorResponseJSONResponse: api.BadRequestErrorResponseJSONResponse{
				Errors: []api.Error{{
					Message: &errors,
				}},
			},
		}, nil
	}

	value, err := json.Marshal(request.Body.Data.Value)
	if err != nil {
		return nil, fmt.Errorf("attribute marshal failed: %w", err)
	}

	id := uuid.New()
	if err := app.db.Queries.AttributeCreate(ctx, &queries.AttributeCreateParams{
		ID:        id,
		ProcessID: process.Id,
		Attribute: request.Body.Data.Attribute,
		Value:     value,
	}); err != nil {
		return nil, fmt.Errorf("attribute create failed: %w", err)
	}

	vwlID := uuid.New()
	if request.Params.FSCVWLVerwerkingsSpan != nil {
		vwlID = *request.Params.FSCVWLVerwerkingsSpan
	}

	return api.CreateAttributeByProcesId201JSONResponse{
		ObservationCreateResponseJSONResponse: api.ObservationCreateResponseJSONResponse{
			Body: api.ObservationCreateResponse{
				Data: api.Observation{
					Attribute: request.Body.Data.Attribute,
					CreatedAt: time.Now(),
					Id:        id,
					Source:    app.source,
					Value:     value,
				},
			},
			Headers: api.ObservationCreateResponseResponseHeaders{
				FSCVWLSysteem:               app.vwlSystem,
				FSCVWLVerantwoordelijke:     app.cfg.Organization.Name,
				FSCVWLVerwerker:             app.cfg.Organization.Name,
				FSCVWLVerwerkingsActiviteit: "https://example.com/activity/v0/vreemdeling-create",
				FSCVWLVerwerkingsSpan:       vwlID,
			},
		},
	}, nil
}

// CreateProcesByVreemdelingenId implements api.StrictServerInterface.
func (app *Application) CreateProcesByVreemdelingenId(ctx context.Context, request api.CreateProcesByVreemdelingenIdRequestObject) (api.CreateProcesByVreemdelingenIdResponseObject, error) {
	if err := validateProcess(request.Body.Data, app.processes); err != nil {
		errors := fmt.Sprintf("validation failed: %s", err.Error())

		return api.CreateProcesByVreemdelingenId400JSONResponse{
			BadRequestErrorResponseJSONResponse: api.BadRequestErrorResponseJSONResponse{
				Errors: []api.Error{
					{
						Message: &errors,
					},
				},
			},
		}, nil
	}

	count, err := app.db.Queries.ProcessCount(ctx)
	if err != nil {
		return nil, fmt.Errorf("process count failed: %s", err)
	}

	processID := fmt.Sprintf("%010d", count+1)

	id := uuid.New()
	if err := app.db.Queries.ProcessCreate(ctx, &queries.ProcessCreateParams{
		ID:            id,
		ProcessID:     processID,
		VreemdelingID: request.VreemdelingId,
		Type:          request.Body.Data.Type,
		Status:        request.Body.Data.Status,
	}); err != nil {
		return nil, fmt.Errorf("process create failed: %w", err)
	}

	attributes := make([]string, 0, len(request.Body.Data.Attributes))
	for _, attribute := range request.Body.Data.Attributes {
		value, err := json.Marshal(attribute.Value)
		if err != nil {
			return nil, fmt.Errorf("attribute marshal failed: %w", err)
		}

		if err := app.db.Queries.AttributeCreate(ctx, &queries.AttributeCreateParams{
			ID:        uuid.New(),
			ProcessID: id,
			Attribute: attribute.Attribute,
			Value:     value,
		}); err != nil {
			return nil, fmt.Errorf("attribute create failed: %w", err)
		}

		attributes = append(attributes, attribute.Attribute)
	}

	data := map[string]any{
		"procesId":      id,
		"vreemdelingId": request.VreemdelingId,
	}

	if err := app.Notify(request.Body.Data.Type, ntCreate, data); err != nil {
		return nil, fmt.Errorf("notify failed: %w", err)
	}

	proces, err := app.readProcesById(ctx, id.String(), &attributes)
	if err != nil {
		return nil, fmt.Errorf("read proces by id failed: %w", err)
	}

	return api.CreateProcesByVreemdelingenId201JSONResponse{
		ProcesCreateResponseJSONResponse: api.ProcesCreateResponseJSONResponse{
			Body: api.ProcesCreateResponse{
				Data: *proces,
			},
			Headers: api.ProcesCreateResponseResponseHeaders{
				FSCVWLSysteem:               app.vwlSystem,
				FSCVWLVerantwoordelijke:     app.cfg.Organization.Name,
				FSCVWLVerwerker:             app.cfg.Organization.Name,
				FSCVWLVerwerkingsActiviteit: "http://example.com/activity/v0/process-create",
				FSCVWLVerwerkingsSpan:       *request.Params.FSCVWLVerwerkingsSpan,
			},
		},
	}, nil
}

// UpdateProcesById implements api.StrictServerInterface.
func (app *Application) UpdateProcesById(ctx context.Context, request api.UpdateProcesByIdRequestObject) (api.UpdateProcesByIdResponseObject, error) {
	process, err := app.readProcesById(ctx, request.ProcesId, nil)
	if err != nil {
		return nil, err
	}

	if err := validateProcesUpdate(request.Body.Data, app.processes[process.Type]); err != nil {
		return nil, err
	}

	if err := app.db.Queries.ProcessUpdate(ctx, &queries.ProcessUpdateParams{
		Status:    request.Body.Data.Status,
		ProcessID: process.ProcesId,
	}); err != nil {
		return nil, err
	}

	data := map[string]any{
		"procesId":      process.Id,
		"vreemdelingId": process.VreemdelingId,
	}

	if err := app.Notify(process.Type, ntUpdate, data); err != nil {
		return nil, fmt.Errorf("notify failed: %w", err)
	}

	return api.UpdateProcesById204Response{}, err
}

// DeleteAttributeById implements api.StrictServerInterface.
func (app *Application) DeleteAttributeById(ctx context.Context, request api.DeleteAttributeByIdRequestObject) (api.DeleteAttributeByIdResponseObject, error) {
	if err := app.db.Queries.AttributeSoftDelete(ctx, request.AttributeId); err != nil {
		return nil, fmt.Errorf("process attributes soft delete failed: %w", err)
	}

	return api.DeleteAttributeById204Response{}, nil
}

func (app *Application) readProcesById(ctx context.Context, processID api.PathProcesId, attributes *[]string) (*api.Proces, error) {
	var record *queries.SigmaProcess

	if id, err := uuid.Parse(processID); err == nil {
		if record, err = app.db.Queries.ProcessGetID(ctx, id); err != nil {
			return nil, fmt.Errorf("process get failed: %w", err)
		}
	} else {
		if record, err = app.db.Queries.ProcessGetPID(ctx, processID); err != nil {
			return nil, fmt.Errorf("process get failed: %w", err)
		}
	}

	process := ToProcess(record, app.source)
	if attributes != nil {
		params := &queries.AttributeListParams{
			ProcessID: record.ID,
			Column2:   *attributes,
		}

		attributeRecords, err := app.db.Queries.AttributeList(ctx, params)
		if err != nil {
			return nil, err
		}

		process.Attributes = ToAttributes(attributeRecords, app.source)
	}

	return process, nil
}

func ToProcess(record *queries.SigmaProcess, source string) *api.Proces {
	var deletedAt *time.Time
	if record.DeletedAt.Valid {
		deletedAt = &record.DeletedAt.Time
	}

	return &api.Proces{
		CreatedAt:     record.CreatedAt,
		DeletedAt:     deletedAt,
		Id:            record.ID,
		ProcesId:      record.ProcessID,
		VreemdelingId: record.VreemdelingID,
		Status:        record.Status,
		Type:          record.Type,
		Source:        source,
	}
}

func ToAttribute(record *queries.SigmaProcessAttribute, source string) *api.Observation {
	var invalidatedAt *time.Time
	if record.DeletedAt.Valid {
		invalidatedAt = &record.DeletedAt.Time
	}

	return &api.Observation{
		Attribute:     record.Attribute,
		CreatedAt:     record.CreatedAt,
		InvalidatedAt: invalidatedAt,
		Id:            record.ID,
		Source:        source,
		Value:         record.Value,
	}
}

func ToAttributes(records []*queries.SigmaProcessAttribute, source string) *map[string]api.Attribute {
	attributes := map[string]api.Attribute{}

	for i, spa := range records {
		a := attributes[spa.Attribute]

		observation := ToAttribute(records[i], source)
		a.Observations = append(a.Observations, *observation)
		a.Value = observation.Value
		attributes[spa.Attribute] = a
	}

	return &attributes
}

func validateProcess(data api.ProcesCreate, processes map[string]Proces) error {
	process, ok := processes[data.Type]
	if !ok {
		return fmt.Errorf("process '%s' does not exists", data.Type)
	}

	if _, ok := process.Statuses[data.Status]; !ok {
		return fmt.Errorf("status '%s' does not exists in process '%s'", data.Status, process.Label)
	}

	for _, attribute := range data.Attributes {
		attr, ok := process.Attributes[attribute.Attribute]
		if !ok {
			return fmt.Errorf("attribute '%s' does not exists in process '%s'", attribute.Attribute, process.Label)
		}

		if err := validateType(attr, attribute.Value); err != nil {
			return fmt.Errorf("validating failed for '%s': %w", attr.Label, err)
		}
	}
	return nil
}

func validateAttribute(data api.ObservationCreate, process Proces) error {
	attribute, ok := process.Attributes[data.Attribute]
	if !ok {
		return fmt.Errorf("attribute '%s' does not exists in process '%s'", data.Attribute, process.Label)
	}

	if err := validateType(attribute, data.Value); err != nil {
		return fmt.Errorf("validation failed for '%s': %w", attribute.Label, err)
	}

	return nil
}

func validateProcesUpdate(data api.ProcesUpdate, process Proces) error {
	if _, ok := process.Statuses[data.Status]; !ok {
		return fmt.Errorf("status '%s' does not exists in process '%s'", data.Status, process.Label)
	}

	return nil
}
