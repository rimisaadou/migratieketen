--
-- PostgreSQL database dump
--

-- Dumped from database version 16.0 (Debian 16.0-1.pgdg110+1)
-- Dumped by pg_dump version 16.0 (Debian 16.0-1.pgdg110+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: contracts; Type: SCHEMA; Schema: -; Owner: -
--

CREATE SCHEMA contracts;


--
-- Name: peers; Type: SCHEMA; Schema: -; Owner: -
--

CREATE SCHEMA peers;


--
-- Name: content_hash_algorithm; Type: TYPE; Schema: contracts; Owner: -
--

CREATE TYPE contracts.content_hash_algorithm AS ENUM (
    'sha3_512'
);


--
-- Name: service_protocol_type; Type: TYPE; Schema: contracts; Owner: -
--

CREATE TYPE contracts.service_protocol_type AS ENUM (
    'PROTOCOL_TCP_HTTP_1.1',
    'PROTOCOL_TCP_HTTP_2'
);


--
-- Name: signature_type; Type: TYPE; Schema: contracts; Owner: -
--

CREATE TYPE contracts.signature_type AS ENUM (
    'accept',
    'reject',
    'revoke'
);


SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: content; Type: TABLE; Schema: contracts; Owner: -
--

CREATE TABLE contracts.content (
    hash character varying(150) NOT NULL,
    hash_algorithm contracts.content_hash_algorithm NOT NULL,
    id uuid NOT NULL,
    group_id character varying(255) NOT NULL,
    valid_not_before timestamp with time zone NOT NULL,
    valid_not_after timestamp with time zone NOT NULL,
    created_at timestamp with time zone NOT NULL
);


--
-- Name: contract_signatures; Type: TABLE; Schema: contracts; Owner: -
--

CREATE TABLE contracts.contract_signatures (
    content_hash character varying(150) NOT NULL,
    signature_type contracts.signature_type NOT NULL,
    peer_id character varying(20) NOT NULL,
    certificate_thumbprint character varying(512) NOT NULL,
    signature character varying(2048) NOT NULL,
    signed_at timestamp with time zone NOT NULL
);


--
-- Name: grants_delegated_service_connection; Type: TABLE; Schema: contracts; Owner: -
--

CREATE TABLE contracts.grants_delegated_service_connection (
    hash character varying(150) NOT NULL,
    content_hash character varying(150) NOT NULL,
    outway_peer_id character varying(20) NOT NULL,
    delegator_peer_id character varying(20) NOT NULL,
    service_peer_id character varying(20) NOT NULL,
    service_name character varying(255) NOT NULL,
    certificate_thumbprint character varying(512) NOT NULL
);


--
-- Name: grants_delegated_service_publication; Type: TABLE; Schema: contracts; Owner: -
--

CREATE TABLE contracts.grants_delegated_service_publication (
    hash character varying(150) NOT NULL,
    content_hash character varying(150) NOT NULL,
    directory_peer_id character varying(20) NOT NULL,
    delegator_peer_id character varying(20) NOT NULL,
    service_peer_id character varying(20) NOT NULL,
    service_name character varying(255) NOT NULL,
    service_protocol contracts.service_protocol_type NOT NULL
);


--
-- Name: grants_service_connection; Type: TABLE; Schema: contracts; Owner: -
--

CREATE TABLE contracts.grants_service_connection (
    hash character varying(150) NOT NULL,
    content_hash character varying(150) NOT NULL,
    consumer_peer_id character varying(20) NOT NULL,
    service_peer_id character varying(20) NOT NULL,
    service_name character varying(255) NOT NULL,
    certificate_thumbprint character varying(512) NOT NULL
);


--
-- Name: grants_service_publication; Type: TABLE; Schema: contracts; Owner: -
--

CREATE TABLE contracts.grants_service_publication (
    hash character varying(150) NOT NULL,
    content_hash character varying(150) NOT NULL,
    directory_peer_id character varying(20) NOT NULL,
    service_peer_id character varying(20) NOT NULL,
    service_name character varying(255) NOT NULL,
    service_protocol contracts.service_protocol_type NOT NULL
);


--
-- Name: valid_contracts; Type: VIEW; Schema: contracts; Owner: -
--

CREATE VIEW contracts.valid_contracts AS
 SELECT hash
   FROM contracts.content content
  WHERE ((valid_not_before < now()) AND (valid_not_after > now()) AND (( SELECT count(*) AS count
           FROM contracts.contract_signatures s
          WHERE ((s.signature_type = 'reject'::contracts.signature_type) AND ((s.content_hash)::text = (content.hash)::text))) = 0) AND (( SELECT count(*) AS count
           FROM contracts.contract_signatures s
          WHERE ((s.signature_type = 'revoke'::contracts.signature_type) AND ((s.content_hash)::text = (content.hash)::text))) = 0) AND (( SELECT count(DISTINCT p.id) AS count
           FROM ( SELECT grants_service_publication.directory_peer_id AS id
                   FROM contracts.grants_service_publication
                  WHERE ((grants_service_publication.content_hash)::text = (content.hash)::text)
                UNION
                 SELECT grants_service_publication.service_peer_id AS id
                   FROM contracts.grants_service_publication
                  WHERE ((grants_service_publication.content_hash)::text = (content.hash)::text)
                UNION
                 SELECT grants_service_connection.consumer_peer_id AS id
                   FROM contracts.grants_service_connection
                  WHERE ((grants_service_connection.content_hash)::text = (content.hash)::text)
                UNION
                 SELECT grants_service_connection.service_peer_id AS id
                   FROM contracts.grants_service_connection
                  WHERE ((grants_service_connection.content_hash)::text = (content.hash)::text)
                UNION
                 SELECT grants_service_connection.service_peer_id AS id
                   FROM contracts.grants_service_connection
                  WHERE ((grants_service_connection.content_hash)::text = (content.hash)::text)
                UNION
                 SELECT grants_delegated_service_connection.service_peer_id AS id
                   FROM contracts.grants_delegated_service_connection
                  WHERE ((grants_delegated_service_connection.content_hash)::text = (content.hash)::text)
                UNION
                 SELECT grants_delegated_service_connection.outway_peer_id AS id
                   FROM contracts.grants_delegated_service_connection
                  WHERE ((grants_delegated_service_connection.content_hash)::text = (content.hash)::text)
                UNION
                 SELECT grants_delegated_service_connection.delegator_peer_id AS id
                   FROM contracts.grants_delegated_service_connection
                  WHERE ((grants_delegated_service_connection.content_hash)::text = (content.hash)::text)
                UNION
                 SELECT grants_delegated_service_publication.service_peer_id AS id
                   FROM contracts.grants_delegated_service_publication
                  WHERE ((grants_delegated_service_publication.content_hash)::text = (content.hash)::text)
                UNION
                 SELECT grants_delegated_service_publication.directory_peer_id AS id
                   FROM contracts.grants_delegated_service_publication
                  WHERE ((grants_delegated_service_publication.content_hash)::text = (content.hash)::text)
                UNION
                 SELECT grants_delegated_service_publication.delegator_peer_id AS id
                   FROM contracts.grants_delegated_service_publication
                  WHERE ((grants_delegated_service_publication.content_hash)::text = (content.hash)::text)) p) = ( SELECT count(*) AS count
           FROM contracts.contract_signatures s
          WHERE ((s.signature_type = 'accept'::contracts.signature_type) AND ((s.content_hash)::text = (content.hash)::text)))));


--
-- Name: certificates; Type: TABLE; Schema: peers; Owner: -
--

CREATE TABLE peers.certificates (
    certificate_thumbprint character varying(512) NOT NULL,
    peer_id character varying(20) NOT NULL,
    certificate bytea NOT NULL
);


--
-- Name: peers; Type: TABLE; Schema: peers; Owner: -
--

CREATE TABLE peers.peers (
    id character varying(20) NOT NULL,
    name character varying(60),
    manager_address character varying(255)
);


--
-- Name: schema_migrations; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.schema_migrations (
    version bigint NOT NULL,
    dirty boolean NOT NULL
);


--
-- Data for Name: content; Type: TABLE DATA; Schema: contracts; Owner: -
--

COPY contracts.content (hash, hash_algorithm, id, group_id, valid_not_before, valid_not_after, created_at) FROM stdin;
$1$1$4dRlYudvIbTbd_CiBu-KFyFNbD-UfRgbHU0Bpf3E6liesxeU7e9TI7Lb-5w3ZfA7CMjm1qnULQALUBezTg5b-A	sha3_512	018ce88a-49d3-74ef-919b-417097ff45b0	fsc-demo	2024-01-08 00:00:00+00	2024-03-03 00:00:00+00	2024-01-08 10:07:06+00
$1$1$8JQs1BS-cTkuplzHSHmBgglWLNC5P_URr63JAWGpbLlqV6mXWz27g4t3eojYCK7lWKkjB6zbW_nCU-w4uBUFBA	sha3_512	018ce88a-6175-74ef-ae67-271d6009a6a0	fsc-demo	2024-01-08 00:00:00+00	2024-03-03 00:00:00+00	2024-01-08 10:07:12+00
$1$1$8-IZNaXDxo6f7NgxPobfDutvgRAHAy2EqXIzgzEyFlZU7T2IZIfI9TbMBLQ-IZkXfFxjigZa9M72YN_Tg5OMBg	sha3_512	018ce88a-7090-74ef-8654-1172fd796f0c	fsc-demo	2024-01-08 00:00:00+00	2024-03-03 00:00:00+00	2024-01-08 10:07:16+00
\.


--
-- Data for Name: contract_signatures; Type: TABLE DATA; Schema: contracts; Owner: -
--

COPY contracts.contract_signatures (content_hash, signature_type, peer_id, certificate_thumbprint, signature, signed_at) FROM stdin;
$1$1$4dRlYudvIbTbd_CiBu-KFyFNbD-UfRgbHU0Bpf3E6liesxeU7e9TI7Lb-5w3ZfA7CMjm1qnULQALUBezTg5b-A	accept	01702331477921460562	r1KCu084pa7bEM1zQSCJe5pehX85Bi4_s7PVtpzNpos	eyJhbGciOiJSUzUxMiIsIng1dCNTMjU2IjoicjFLQ3UwODRwYTdiRU0xelFTQ0plNXBlaFg4NUJpNF9zN1BWdHB6TnBvcyJ9.eyJjb250ZW50SGFzaCI6IiQxJDEkNGRSbFl1ZHZJYlRiZF9DaUJ1LUtGeUZOYkQtVWZSZ2JIVTBCcGYzRTZsaWVzeGVVN2U5VEk3TGItNXczWmZBN0NNam0xcW5VTFFBTFVCZXpUZzViLUEiLCJ0eXBlIjoiYWNjZXB0Iiwic2lnbmVkQXQiOjE3MDQ3MDg0MjZ9.udyqmkBeREp8AXFe7DvOluaa4YjzPQlzsXoMVI7LgmOEkriglhIkRtPhstyJnH4SbQvky4FgEkvMJM-mb-dRv9l_vJrGRRcYu_Gs3BR4YNtl8k6wwrFHYv63IX3aY8X-_Cgrh5Owc1vhYdN7umulFSq0UxNXXfosywh7NHUYXwwu35IugK8Q3vq2rCnH8h_W5OkvcGLlR4RMtUQfDMH8bIW0gCA4dNrB5iMfpXdZDWxA6Q87XgQIqeYbHGmqgEB8ThQdj0Hf7Ih7jtDa_OCYfhfRJMvb9YWkLuhfzZSQw9QJCsb_KTjXn18MQZ6Y-XGuseRII_flspOjiNVT5oPmr0N6KX4abr_AAJJlXFKQYzy8w6oN47ddSf08RrfZysgzmdVdce57v1hDx6YpO_oLrM4gWsoV0ihG-jCQtNS5HrwF2JVpbzPxUKGWhjv2UPts0Pvhr1vVT3_azXwooexLv0GXzR-D-IvzivVHgNWskqOnxIR63SQpzBxf90JDit4nI9Y2h2cX2P2mT3_1nRHinurlF5uin1y0g4E-Hc_8rCsB3cxbUBI9RX9rvxZRmvhfKnR4u2dEE-k8Xs0Zg_u1uH9mCZFOBbM2icoTig5_lMYQYyUo3FehW-8aWI4G03_7Npbm6zhrO2tmCNHt_6RAE1VZ9T8MhRaL6_5iTnV1vw4	2024-01-08 10:07:06.198601+00
$1$1$8JQs1BS-cTkuplzHSHmBgglWLNC5P_URr63JAWGpbLlqV6mXWz27g4t3eojYCK7lWKkjB6zbW_nCU-w4uBUFBA	accept	01702331477921460562	r1KCu084pa7bEM1zQSCJe5pehX85Bi4_s7PVtpzNpos	eyJhbGciOiJSUzUxMiIsIng1dCNTMjU2IjoicjFLQ3UwODRwYTdiRU0xelFTQ0plNXBlaFg4NUJpNF9zN1BWdHB6TnBvcyJ9.eyJjb250ZW50SGFzaCI6IiQxJDEkOEpRczFCUy1jVGt1cGx6SFNIbUJnZ2xXTE5DNVBfVVJyNjNKQVdHcGJMbHFWNm1YV3oyN2c0dDNlb2pZQ0s3bFdLa2pCNnpiV19uQ1UtdzR1QlVGQkEiLCJ0eXBlIjoiYWNjZXB0Iiwic2lnbmVkQXQiOjE3MDQ3MDg0MzJ9.NFTR5S1UJqfHo0E1JY9xPrVWcc32kliQtN-3pIfGln8dNxK3WRpfXTysM-y8UdiF3OBor0P8rWKEBnqgUm9Nv8oHuc2hksKizUVrgWywiS7EwfaTrqP9Z4Ll4pez5Pa_MHMPmHJfB2gFYeSvgkeFEc79f26tq-5s4qvFoyLQMmuLdPp0pZVUo4nQbV8MZmiRotbbvABTMCBBsSW7E9eETDKOQP0PwP1ES2zrziC_y_h25L0E9_Ds-e90o2BPDD11QyA5MIdpvMljTY4yK2mR_varTmTjTmj-rXGIsDowcwOuO6JvTrCzaJ8N5t6eimPsEJQ6-cF7n8S1ea9YBneYA66juSv7cTZrR-df7DnRJoKtcSs7McOmrDP8iMcVvdF8b9Z3LUg2yVJwG2AOV-Va2H7kronf3v6WSXQxkAKUfJNSBkFiYgb59sP0AUgW9zdnqOUCU57xE2V5RqAIf7q0CJY-iaWsRHLATiYfLDQDT2hUs7m2kYv5dhajKH1a855-J7TVwoDZDUqCZHP0AYhAYxHUCcU__C8ZJEcoS7-SjE76IKkoDnIOJPNCnCC_9fPAu1INnbvwV6ZBZT7itVv_qnCViv-vH3Yi8saNxeo0J90N7RIxzGzn2pg9XS4zYKIzCD_n-R5KvfPCEDUBmoJiDc0jx36MiCU3X-Sj1rraX1U	2024-01-08 10:07:12.246427+00
$1$1$8-IZNaXDxo6f7NgxPobfDutvgRAHAy2EqXIzgzEyFlZU7T2IZIfI9TbMBLQ-IZkXfFxjigZa9M72YN_Tg5OMBg	accept	01702331477921460562	r1KCu084pa7bEM1zQSCJe5pehX85Bi4_s7PVtpzNpos	eyJhbGciOiJSUzUxMiIsIng1dCNTMjU2IjoicjFLQ3UwODRwYTdiRU0xelFTQ0plNXBlaFg4NUJpNF9zN1BWdHB6TnBvcyJ9.eyJjb250ZW50SGFzaCI6IiQxJDEkOC1JWk5hWER4bzZmN05neFBvYmZEdXR2Z1JBSEF5MkVxWEl6Z3pFeUZsWlU3VDJJWklmSTlUYk1CTFEtSVprWGZGeGppZ1phOU03MllOX1RnNU9NQmciLCJ0eXBlIjoiYWNjZXB0Iiwic2lnbmVkQXQiOjE3MDQ3MDg0MzZ9.oAYwl55cp0dGcIw2WP0RE0AxECKXY4N1jnb2FsMvqBQrPTnl-Y10coRJN9Zt8sYvosgcvZsZzFZ1fXRLfRREDmS9Wl6X39NOKFe4-qm6SfmwR38K0HXrX9b_q9qmw8awMicJ4v6ED3dQJ0Mflf0rO3lu_GPmqha2x_OKVM_AJPIe761NqjZbPDoxE76gelW7z7gZaHzyulMQmP-xB0ZZi4ZbNaNLzkRgBfYNJ2ojFYxxqvTMxALxd6PC5N2JvdauQIdgitVZbKUReWAKodTyo8qy5j2AndgPXj6Cf9NG_7duSL8w3PYUx2zeTAE_qkOiMgmqRX-AyS89CPdfwzfs9-HJ_fFzsn9sfRhE2lRucZyfCkNkFg7-wjQ_9CPPBmx-8W-Uny1AzixhQJ1O6JqkMYl5ISjxBACX_O_M52MZDedl32f6RN3ym8i9oYeDqIp5ZDawh4ONDYVG8R5h6KrpVqxadGGtZj4YzFXI0-8M1SyiuMmdT7vI49KmaqkXvdLovMQJ3IwvwwYfq41-VzCgbs4h3lfQtjvabJqcKKPaKoJ_AKTF32gKRWlVbcs0zNTX1U4SRJPcEtXly2SgdJJyx7H7_Lbcil58cJNx4LfzwGJFpusIq07XfTMuTmDAeQ_dduFPXGToEeD5O68s_X5cVkJygL5Tbc3nL-bDCzaIXUI	2024-01-08 10:07:16.113326+00
\.


--
-- Data for Name: grants_delegated_service_connection; Type: TABLE DATA; Schema: contracts; Owner: -
--

COPY contracts.grants_delegated_service_connection (hash, content_hash, outway_peer_id, delegator_peer_id, service_peer_id, service_name, certificate_thumbprint) FROM stdin;
\.


--
-- Data for Name: grants_delegated_service_publication; Type: TABLE DATA; Schema: contracts; Owner: -
--

COPY contracts.grants_delegated_service_publication (hash, content_hash, directory_peer_id, delegator_peer_id, service_peer_id, service_name, service_protocol) FROM stdin;
\.


--
-- Data for Name: grants_service_connection; Type: TABLE DATA; Schema: contracts; Owner: -
--

COPY contracts.grants_service_connection (hash, content_hash, consumer_peer_id, service_peer_id, service_name, certificate_thumbprint) FROM stdin;
$1$3$XbGPbadexkwAfXwhYcW70WDJX7yvlFQ48FJoAOjVu9lpHySoWlWXPbqEt1RqnDonbS50nlO-QZdJZgNz5pA29g	$1$1$4dRlYudvIbTbd_CiBu-KFyFNbD-UfRgbHU0Bpf3E6liesxeU7e9TI7Lb-5w3ZfA7CMjm1qnULQALUBezTg5b-A	01702331477921460562	01702331477921460562	fictief-np-bvv-backend	r1KCu084pa7bEM1zQSCJe5pehX85Bi4_s7PVtpzNpos
$1$3$RAczmt28ai96zcXDBaLBVTL8n4aPtx4w-9KMJZ6tRsK2XIcpSmnBeQCP0j8PKeqjIkv_NZ2JufiAUtnapoPOIA	$1$1$8JQs1BS-cTkuplzHSHmBgglWLNC5P_URr63JAWGpbLlqV6mXWz27g4t3eojYCK7lWKkjB6zbW_nCU-w4uBUFBA	01702331477921460562	01702331477921460562	fictief-np-sigma-backend	r1KCu084pa7bEM1zQSCJe5pehX85Bi4_s7PVtpzNpos
$1$3$AuPnjI0wTT3Yb9TP332wylvRSRONh6gIUwYh5cSYY_BqCzfr0jaxXeCTAiWI1v3ih3d4G121pj626xqOb3J5yA	$1$1$8-IZNaXDxo6f7NgxPobfDutvgRAHAy2EqXIzgzEyFlZU7T2IZIfI9TbMBLQ-IZkXfFxjigZa9M72YN_Tg5OMBg	01702331477921460562	01702331477921460562	fictief-np-loket-backend	r1KCu084pa7bEM1zQSCJe5pehX85Bi4_s7PVtpzNpos
\.


--
-- Data for Name: grants_service_publication; Type: TABLE DATA; Schema: contracts; Owner: -
--

COPY contracts.grants_service_publication (hash, content_hash, directory_peer_id, service_peer_id, service_name, service_protocol) FROM stdin;
\.


--
-- Data for Name: certificates; Type: TABLE DATA; Schema: peers; Owner: -
--

COPY peers.certificates (certificate_thumbprint, peer_id, certificate) FROM stdin;
r1KCu084pa7bEM1zQSCJe5pehX85Bi4_s7PVtpzNpos	01702331477921460562	\\x308206673082044fa003020102021415a4f7f10bcb2f410ad97eaa863de64e4d999009300d06092a864886f70d01010d0500305f310b3009060355040613024e4c311630140603550408130d4e6f6f72642d486f6c6c616e643112301006035504071309416d7374657264616d31163014060355040a130d436f6d6d6f6e2047726f756e64310c300a060355040b13034e4c58301e170d3233313231313231343630305a170d3236313231303231343630305a30819a310b3009060355040613024e4c3110300e06035504081307557472656368743110300e0603550407130755747265636874310f300d060355040a1306736861726564313730350603550403132e6d616e616765722d6673632d6e6c782d6d616e616765722d65787465726e616c2e6d696772617469656b6574656e311d301b06035504051314303137303233333134373739323134363035363230820222300d06092a864886f70d01010105000382020f003082020a0282020100dba78a07eac843512a2b82df33c30f56bde128ff35b5cd6ef6ff02183515eb22460b8672d535a335489d4c96b5ccf69a0f273c3eddbf8cc7914f3f7404e43f94c5031bc7623984407b545f98ddee80f398b595a39c425f56b5f95cc2551b56f7a9dde01d33422bb93450858732048f2de770af2c95bbe8beac93c1621e40db9178ed1f7befa11191131609a1e87072cc251a0e073eb0fbf262665b1c13eb913231e893b3a5ccfce235934bbed108f10b98dbe92bbecb71e1133a3c9115b1f2727c5b823e8a62e1522392bcba52141d7a66e7e589ff2e7296364d0341343f9dbbab1aeb536d6bfd21b022c0fd5771c286752b79434d6780fe342b12f197c44cf074380605bf26d6c5df8b022ba2a889baba11ca4b1a785366b86deb6911e71f2e11516653f42e0a3f5b568ba355f6c89bb41b9ec54149dd849ad76c466b5517bb39f0f4b0d90b8f5351c6acf1520113abe0eaa9f540d2c9a2b14e80a41f6182fa8783de71a1989f6a84eabe24416afa009442f4bffa57f1bbdfaa4cf2401c1a9037baf60977f66af98a6ead7dc4f677f923e783135f9106a6ec1ea71d6cafd846640e3f7494686125e996c5d4c56db7cdd7afbc9d6f971122d8da82631859caa4ce4009327901863aa88c94379906ae6424b1b94a1e239ce916b406b4263dec3fa8c8ddb3512b65be8f6130aa4d8c5ac926ab91ca65e69920203bdd569c10464b0203010001a381de3081db300e0603551d0f0101ff0404030205a0301d0603551d250416301406082b0601050507030106082b06010505070302300c0603551d130101ff04023000301d0603551d0e041604143826a7472b7e3d568ea1774ea93807baf407acdc301f0603551d230418301680145ae041511187acda6887c000c3065ce2792d89fd305c0603551d1104553053822e6d616e616765722d6673632d6e6c782d6d616e616765722d65787465726e616c2e6d696772617469656b6574656e8221696e7761792d6673632d6e6c782d696e7761792e6d696772617469656b6574656e300d06092a864886f70d01010d050003820201009b2d941e1841446a937132c9bc44ed85a31836f990adfb296b861e5aa0d4b51ffc146ea83b64d0df751a35943b849b3f6ff8c67978897f5fd992bc782748134da4075f405e6cb249d39e2bcd9f8b59b10a5735aff0e9ae6d918f8634dec777d3c80c31d075a9939a89f33351460c0bd35bbe5e5e3460d26ff94d084d324e60316cd2b6b716543b6bba61d3f8aa18084e4cab92952275bc81a92d215f086b31c0c3dacce8a85b3c4187686deda0a3277ca0abfada672d6a6f5a02771954147990d817dfba285c96a3a443c71e052a1617c951b20a3e349c64cc3f8d2e055e7baca485356be40599348f8cb9def5707bb386502a051fa69ee942aee06c5010f9652887783706d57af033c93de6375797936e05ac52d5c018fa9398851e5a590dc141e451e101e7f592f0ad01ba6d462444e205ce7b25411a9a17ed51914e5ddad17ac246ae7bfe6c649a2e1160c6784df8be5111652063d605c12c018d0ac874b1a71151b72efce6a6a84a77c607f2f271277eb8dca5395baf183a482df3fd3cf3123ab705818dd02de80af5ff8fb53dfcc3fc87a383459a3dd80c77189f8f21f7ca40d7c44c28588b7b48e1e6e7d170507a937669440a909d18c23644c2d4734f392a994df803ccfb204decf9ddd8d9b325231bc3ec949b218daff99b4486e4223c05ca5e2a53ec498873a9ef14a5b437285c395b17600d92fccbab57057ca108
\.


--
-- Data for Name: peers; Type: TABLE DATA; Schema: peers; Owner: -
--

COPY peers.peers (id, name, manager_address) FROM stdin;
01702331477921460562	shared	https://manager-fsc-nlx-manager-external.migratieketen:8443
12345678901234567899	rijksdienst-realistische-demos	https://manager-fsc-nlx-manager-external.rrd:8443
\.


--
-- Data for Name: schema_migrations; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.schema_migrations (version, dirty) FROM stdin;
1	f
\.


--
-- Name: content content_pk; Type: CONSTRAINT; Schema: contracts; Owner: -
--

ALTER TABLE ONLY contracts.content
    ADD CONSTRAINT content_pk PRIMARY KEY (hash);


--
-- Name: contract_signatures contract_signatures_pk; Type: CONSTRAINT; Schema: contracts; Owner: -
--

ALTER TABLE ONLY contracts.contract_signatures
    ADD CONSTRAINT contract_signatures_pk PRIMARY KEY (content_hash, signature_type, peer_id);


--
-- Name: grants_delegated_service_connection grants_delegated_service_connection_pk; Type: CONSTRAINT; Schema: contracts; Owner: -
--

ALTER TABLE ONLY contracts.grants_delegated_service_connection
    ADD CONSTRAINT grants_delegated_service_connection_pk PRIMARY KEY (hash);


--
-- Name: grants_delegated_service_publication grants_delegated_service_publication_pk; Type: CONSTRAINT; Schema: contracts; Owner: -
--

ALTER TABLE ONLY contracts.grants_delegated_service_publication
    ADD CONSTRAINT grants_delegated_service_publication_pk PRIMARY KEY (hash);


--
-- Name: grants_service_connection grants_service_connection_pk; Type: CONSTRAINT; Schema: contracts; Owner: -
--

ALTER TABLE ONLY contracts.grants_service_connection
    ADD CONSTRAINT grants_service_connection_pk PRIMARY KEY (hash);


--
-- Name: grants_service_publication grants_service_publication_pk; Type: CONSTRAINT; Schema: contracts; Owner: -
--

ALTER TABLE ONLY contracts.grants_service_publication
    ADD CONSTRAINT grants_service_publication_pk PRIMARY KEY (hash);


--
-- Name: certificates certificates_pk; Type: CONSTRAINT; Schema: peers; Owner: -
--

ALTER TABLE ONLY peers.certificates
    ADD CONSTRAINT certificates_pk PRIMARY KEY (certificate_thumbprint);


--
-- Name: peers peers_pk; Type: CONSTRAINT; Schema: peers; Owner: -
--

ALTER TABLE ONLY peers.peers
    ADD CONSTRAINT peers_pk PRIMARY KEY (id);


--
-- Name: schema_migrations schema_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.schema_migrations
    ADD CONSTRAINT schema_migrations_pkey PRIMARY KEY (version);


--
-- Name: grants_delegated_service_connection_content_hash_idx; Type: INDEX; Schema: contracts; Owner: -
--

CREATE INDEX grants_delegated_service_connection_content_hash_idx ON contracts.grants_delegated_service_connection USING btree (content_hash);


--
-- Name: grants_delegated_service_connection_delegator_peer_id_idx; Type: INDEX; Schema: contracts; Owner: -
--

CREATE INDEX grants_delegated_service_connection_delegator_peer_id_idx ON contracts.grants_delegated_service_connection USING btree (delegator_peer_id);


--
-- Name: grants_delegated_service_connection_outway_peer_id_idx; Type: INDEX; Schema: contracts; Owner: -
--

CREATE INDEX grants_delegated_service_connection_outway_peer_id_idx ON contracts.grants_delegated_service_connection USING btree (outway_peer_id);


--
-- Name: grants_delegated_service_connection_service_peer_id_idx; Type: INDEX; Schema: contracts; Owner: -
--

CREATE INDEX grants_delegated_service_connection_service_peer_id_idx ON contracts.grants_delegated_service_connection USING btree (service_peer_id);


--
-- Name: grants_service_connection_consumer_peer_id_idx; Type: INDEX; Schema: contracts; Owner: -
--

CREATE INDEX grants_service_connection_consumer_peer_id_idx ON contracts.grants_service_connection USING btree (consumer_peer_id);


--
-- Name: grants_service_connection_content_hash_idx; Type: INDEX; Schema: contracts; Owner: -
--

CREATE INDEX grants_service_connection_content_hash_idx ON contracts.grants_service_connection USING btree (content_hash);


--
-- Name: grants_service_connection_service_peer_id_idx; Type: INDEX; Schema: contracts; Owner: -
--

CREATE INDEX grants_service_connection_service_peer_id_idx ON contracts.grants_service_connection USING btree (service_peer_id);


--
-- Name: grants_service_publication_directory_peer_id_idx; Type: INDEX; Schema: contracts; Owner: -
--

CREATE INDEX grants_service_publication_directory_peer_id_idx ON contracts.grants_service_publication USING btree (directory_peer_id);


--
-- Name: grants_service_publication_service_peer_id_idx; Type: INDEX; Schema: contracts; Owner: -
--

CREATE INDEX grants_service_publication_service_peer_id_idx ON contracts.grants_service_publication USING btree (service_peer_id);


--
-- Name: certificates_certificate_thumbprint_idx; Type: INDEX; Schema: peers; Owner: -
--

CREATE INDEX certificates_certificate_thumbprint_idx ON peers.certificates USING btree (certificate_thumbprint);


--
-- Name: certificates_peer_id_idx; Type: INDEX; Schema: peers; Owner: -
--

CREATE INDEX certificates_peer_id_idx ON peers.certificates USING btree (peer_id);


--
-- Name: contract_signatures contract_signatures_content_hash_fk; Type: FK CONSTRAINT; Schema: contracts; Owner: -
--

ALTER TABLE ONLY contracts.contract_signatures
    ADD CONSTRAINT contract_signatures_content_hash_fk FOREIGN KEY (content_hash) REFERENCES contracts.content(hash) MATCH FULL ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: contract_signatures contract_signatures_peer_id_fk; Type: FK CONSTRAINT; Schema: contracts; Owner: -
--

ALTER TABLE ONLY contracts.contract_signatures
    ADD CONSTRAINT contract_signatures_peer_id_fk FOREIGN KEY (peer_id) REFERENCES peers.peers(id) MATCH FULL ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: grants_delegated_service_connection grants_delegated_service_connection_content_hash_fk; Type: FK CONSTRAINT; Schema: contracts; Owner: -
--

ALTER TABLE ONLY contracts.grants_delegated_service_connection
    ADD CONSTRAINT grants_delegated_service_connection_content_hash_fk FOREIGN KEY (content_hash) REFERENCES contracts.content(hash) MATCH FULL ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: grants_delegated_service_connection grants_delegated_service_connection_delegator_peer_id_fk; Type: FK CONSTRAINT; Schema: contracts; Owner: -
--

ALTER TABLE ONLY contracts.grants_delegated_service_connection
    ADD CONSTRAINT grants_delegated_service_connection_delegator_peer_id_fk FOREIGN KEY (delegator_peer_id) REFERENCES peers.peers(id) MATCH FULL ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: grants_delegated_service_connection grants_delegated_service_connection_outway_peer_id_fk; Type: FK CONSTRAINT; Schema: contracts; Owner: -
--

ALTER TABLE ONLY contracts.grants_delegated_service_connection
    ADD CONSTRAINT grants_delegated_service_connection_outway_peer_id_fk FOREIGN KEY (outway_peer_id) REFERENCES peers.peers(id) MATCH FULL ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: grants_delegated_service_connection grants_delegated_service_connection_service_peer_id_fk; Type: FK CONSTRAINT; Schema: contracts; Owner: -
--

ALTER TABLE ONLY contracts.grants_delegated_service_connection
    ADD CONSTRAINT grants_delegated_service_connection_service_peer_id_fk FOREIGN KEY (service_peer_id) REFERENCES peers.peers(id) MATCH FULL ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: grants_service_connection grants_service_connection_content_hash_fk; Type: FK CONSTRAINT; Schema: contracts; Owner: -
--

ALTER TABLE ONLY contracts.grants_service_connection
    ADD CONSTRAINT grants_service_connection_content_hash_fk FOREIGN KEY (content_hash) REFERENCES contracts.content(hash) MATCH FULL ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: grants_service_connection grants_service_connections_consumer_peer_id_fk; Type: FK CONSTRAINT; Schema: contracts; Owner: -
--

ALTER TABLE ONLY contracts.grants_service_connection
    ADD CONSTRAINT grants_service_connections_consumer_peer_id_fk FOREIGN KEY (consumer_peer_id) REFERENCES peers.peers(id) MATCH FULL ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: grants_service_connection grants_service_connections_service_peer_id_fk; Type: FK CONSTRAINT; Schema: contracts; Owner: -
--

ALTER TABLE ONLY contracts.grants_service_connection
    ADD CONSTRAINT grants_service_connections_service_peer_id_fk FOREIGN KEY (service_peer_id) REFERENCES peers.peers(id) MATCH FULL ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: grants_service_publication grants_service_publication_directory_peer_id_fk; Type: FK CONSTRAINT; Schema: contracts; Owner: -
--

ALTER TABLE ONLY contracts.grants_service_publication
    ADD CONSTRAINT grants_service_publication_directory_peer_id_fk FOREIGN KEY (directory_peer_id) REFERENCES peers.peers(id) MATCH FULL ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: grants_service_publication grants_service_publication_service_peer_id_fk; Type: FK CONSTRAINT; Schema: contracts; Owner: -
--

ALTER TABLE ONLY contracts.grants_service_publication
    ADD CONSTRAINT grants_service_publication_service_peer_id_fk FOREIGN KEY (service_peer_id) REFERENCES peers.peers(id) MATCH FULL ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: certificates certificates_peer_id_fk; Type: FK CONSTRAINT; Schema: peers; Owner: -
--

ALTER TABLE ONLY peers.certificates
    ADD CONSTRAINT certificates_peer_id_fk FOREIGN KEY (peer_id) REFERENCES peers.peers(id) MATCH FULL ON DELETE CASCADE;


--
-- PostgreSQL database dump complete
--

