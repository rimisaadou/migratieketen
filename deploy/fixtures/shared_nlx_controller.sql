--
-- PostgreSQL database dump
--

-- Dumped from database version 16.0 (Debian 16.0-1.pgdg110+1)
-- Dumped by pg_dump version 16.0 (Debian 16.0-1.pgdg110+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: controller; Type: SCHEMA; Schema: -; Owner: -
--

CREATE SCHEMA controller;


SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: inways; Type: TABLE; Schema: controller; Owner: -
--

CREATE TABLE controller.inways (
    group_id character varying(150) NOT NULL,
    name character varying(255) NOT NULL,
    address character varying(2000) NOT NULL
);


--
-- Name: outways; Type: TABLE; Schema: controller; Owner: -
--

CREATE TABLE controller.outways (
    group_id character varying(150) NOT NULL,
    name character varying(100) NOT NULL,
    certificate_thumbprint character varying(4096) NOT NULL
);


--
-- Name: services; Type: TABLE; Schema: controller; Owner: -
--

CREATE TABLE controller.services (
    group_id character varying(150) NOT NULL,
    name character varying(150) NOT NULL,
    endpoint_url character varying(2000) NOT NULL,
    inway_address character varying(2000) NOT NULL
);


--
-- Name: schema_migrations; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.schema_migrations (
    version bigint NOT NULL,
    dirty boolean NOT NULL
);


--
-- Data for Name: inways; Type: TABLE DATA; Schema: controller; Owner: -
--

COPY controller.inways (group_id, name, address) FROM stdin;
fsc-demo	shared-fsc-nlx-inway	https://inway-fsc-nlx-inway.migratieketen:443
\.


--
-- Data for Name: outways; Type: TABLE DATA; Schema: controller; Owner: -
--

COPY controller.outways (group_id, name, certificate_thumbprint) FROM stdin;
fsc-demo	outway-fsc-nlx-outway	r1KCu084pa7bEM1zQSCJe5pehX85Bi4_s7PVtpzNpos
\.


--
-- Data for Name: services; Type: TABLE DATA; Schema: controller; Owner: -
--

COPY controller.services (group_id, name, endpoint_url, inway_address) FROM stdin;
fsc-demo	fictief-np-sigma-backend	http://np-sigma-backend-svc	https://inway-fsc-nlx-inway.migratieketen:443
fsc-demo	fictief-np-bvv-backend	http://np-bvv-backend-svc	https://inway-fsc-nlx-inway.migratieketen:443
fsc-demo	fictief-np-loket-backend	http://shared-loket-backend-svc	https://inway-fsc-nlx-inway.migratieketen:443
fsc-demo	fictief-coa-loket-backend	http://shared-loket-backend-svc	https://inway-fsc-nlx-inway.migratieketen:443
fsc-demo	fictief-div-io-loket-backend	http://shared-loket-backend-svc	https://inway-fsc-nlx-inway.migratieketen:443
fsc-demo	fictief-dji-loket-backend	http://shared-loket-backend-svc	https://inway-fsc-nlx-inway.migratieketen:443
fsc-demo	fictief-dtv-loket-backend	http://shared-loket-backend-svc	https://inway-fsc-nlx-inway.migratieketen:443
fsc-demo	fictief-ind-loket-backend	http://shared-loket-backend-svc	https://inway-fsc-nlx-inway.migratieketen:443
fsc-demo	fictief-rechtspraak-loket-backend	http://shared-loket-backend-svc	https://inway-fsc-nlx-inway.migratieketen:443
\.


--
-- Data for Name: schema_migrations; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.schema_migrations (version, dirty) FROM stdin;
1	f
\.


--
-- Name: inways inways_pk; Type: CONSTRAINT; Schema: controller; Owner: -
--

ALTER TABLE ONLY controller.inways
    ADD CONSTRAINT inways_pk PRIMARY KEY (group_id, name);


--
-- Name: outways outways_name_key; Type: CONSTRAINT; Schema: controller; Owner: -
--

ALTER TABLE ONLY controller.outways
    ADD CONSTRAINT outways_name_key UNIQUE (name);


--
-- Name: outways outways_pk; Type: CONSTRAINT; Schema: controller; Owner: -
--

ALTER TABLE ONLY controller.outways
    ADD CONSTRAINT outways_pk PRIMARY KEY (group_id, name);


--
-- Name: services services_pk; Type: CONSTRAINT; Schema: controller; Owner: -
--

ALTER TABLE ONLY controller.services
    ADD CONSTRAINT services_pk PRIMARY KEY (group_id, name);


--
-- Name: schema_migrations schema_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.schema_migrations
    ADD CONSTRAINT schema_migrations_pkey PRIMARY KEY (version);


--
-- PostgreSQL database dump complete
--

