package gmkapi.gmkapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GmkApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(GmkApiApplication.class, args);
	}

}
