package gmkapi.gmkapi.rest;

import gmkapi.gmkapi.data.*;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
public class GmkController {

    private GmkReader gmkReader;

    public GmkController() {
        gmkReader = new GmkReader();
    }

    @GetMapping(value="/api/gmk/{uuid}")
    public @ResponseBody ResponseEntity<?> getGmkResourcesFromSource(@PathVariable String uuid) {
        try {
            GmkResources gmkResources = gmkReader.readGmkResourcesFromSource(uuid);
            if (gmkResources == null) {
                return new ResponseEntity<CustomErrorType>(createCustomError("GMK with id= " + uuid + " is not available"), HttpStatus.NOT_FOUND);
            }
            return new ResponseEntity<GmkResources>(gmkResources, HttpStatus.OK);
        } catch (Exception ioException) {
            return new ResponseEntity<CustomErrorType>(createCustomError(ioException.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/api/gmk/{context}/{naam}")
    public ResponseEntity<?> getGmkResourcesFromContextNaam(@PathVariable String context, @PathVariable String naam) {
        try {
            GmkResources gmkResources = gmkReader.readGmkResourcesFromContextNaam(context, naam);
            if (gmkResources == null) {
                return new ResponseEntity<CustomErrorType>(createCustomError("GMK with context= " + context + "and naam= " + naam + " is not available"), HttpStatus.NOT_FOUND);
            }
            return new ResponseEntity<GmkResources>(gmkResources, HttpStatus.OK);
        } catch (Exception ioException) {
            return new ResponseEntity<CustomErrorType>(createCustomError(ioException.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    private CustomErrorType createCustomError(String message) {
        CustomErrorType cet = new CustomErrorType();
        cet.setErrorMessage(message);
        return cet;
    }

}
