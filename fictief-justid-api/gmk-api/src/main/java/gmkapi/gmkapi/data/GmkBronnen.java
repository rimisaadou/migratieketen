package gmkapi.gmkapi.data;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@RequiredArgsConstructor
@Getter
@Setter
@ToString
public class GmkBronnen {
    private String externeReferentie;
    private String link;
    private String type;
    private String uri;
    private String value;
}
