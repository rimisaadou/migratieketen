package gmkapi.gmkapi.data;

import net.minidev.json.JSONArray;
import com.jayway.jsonpath.JsonPath;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Objects;

@Service
public class GmkReader {

    final String jsonContent;

    public GmkReader() {
        String jsonContent1;
        try {
            jsonContent1 = getGmkJsonResource();
        } catch (Exception e) {
            jsonContent1 = "Not Initialized";
        }
        jsonContent = jsonContent1;
    }

    private String getGmkJsonResource() {
        StringBuilder json = new StringBuilder();
        try {
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(Objects.requireNonNull(getClass().getClassLoader().getResourceAsStream("gmk.json")),
                            StandardCharsets.UTF_8));
            String str;
            while ((str = in.readLine()) != null)
                json.append(str);
            in.close();
        } catch (IOException e) {
            throw new RuntimeException("Caught exception reading resource \"gmk.json\"", e);
        }
        return json.toString();
    }

    @SuppressWarnings("unchecked")
    public GmkResources readGmkResourcesFromContextNaam(String context, String naam) {
        //String jpath = "$.resources.*[?(@.preflabel == '"+naam+"'), ?(@.contexts == '"+context+"')]";
        String jpath = "$.resources.*[?(@.preflabel == '"+naam+"')]";
        var jsonArray = (JSONArray) readJsonPathContentAsObject(jsonContent, jpath);
        if (jsonArray == null || jsonArray.isEmpty())
            return new GmkResources();
        LinkedHashMap<String, Object> items = (LinkedHashMap<String, Object>) jsonArray.get(0);

        GmkResources gmkResources = readGmkResourcesFromJpath(items);
        gmkResources.setUniekeIdentifier("To do...");
        return gmkResources;
    }

    public GmkResources readGmkResourcesFromSource(String uuid) {
        String jpath = "$.resources."+uuid;
        @SuppressWarnings("unchecked") LinkedHashMap<String, Object> items = (LinkedHashMap<String, Object>) readJsonPathContentAsObject(jsonContent, jpath);
        GmkResources gmkResources = readGmkResourcesFromJpath(items);
        gmkResources.setUniekeIdentifier(uuid);
        return gmkResources;
    }

    public GmkResources readGmkResourcesFromJpath(LinkedHashMap<String, Object> items) {
        GmkResources gmkResources = new GmkResources();
        if (items == null ) return gmkResources;

        gmkResources.setNaam(getValueFromObject(items.get("preflabel")));
        gmkResources.setAlternatieveAanduiding(getValueFromObject(items.get("altlabels")));
        gmkResources.setDefinitie(getValueFromObject(items.get("definition")));
        gmkResources.setStatus(getValueFromObject(items.get("status")));
        gmkResources.setBronnen(getGmkBronnenFromObject(items.get("bronnen")));

        return gmkResources;
    }

    public GmkBronnen readGmkBronnenFromSource(String uuid) {
        String jpath = "$.bronnen."+uuid;
        @SuppressWarnings("unchecked") LinkedHashMap<String, Object> items = (LinkedHashMap<String, Object>) readJsonPathContentAsObject(jsonContent, jpath);
        GmkBronnen gmkBronnen = new GmkBronnen();
        if (items == null ) return gmkBronnen;

        gmkBronnen.setExterneReferentie(getValueFromObject(items.get("externe_referentie")));
        gmkBronnen.setLink(getValueFromObject(items.get("link")));
        gmkBronnen.setType(getValueFromObject(items.get("type")));
        gmkBronnen.setUri(getValueFromObject(items.get("uri")));
        gmkBronnen.setValue(getValueFromObject(items.get("value")));

        return gmkBronnen;
    }

    private List<GmkBronnen> getGmkBronnenFromObject(Object value) {
        List<GmkBronnen> gmkBronnenList = new ArrayList<>();
        String bronnen = getValueFromObject(value);
        for (String bron : bronnen.split(",")) {
            gmkBronnenList.add(readGmkBronnenFromSource(bron));
        }
        return gmkBronnenList;
    }

    private String getValueFromObject(Object value) {
        if (value instanceof JSONArray jsonArray) {
            List<String> arr = new ArrayList<>();
            for (Object o : jsonArray) {
                arr.add(o.toString());
            }
            return String.join(", ", arr);
        }
        return value.toString();
    }

    private static Object readJsonPathContentAsObject(String content, String jPath) {
        return isJPathInJson(content, jPath) ? JsonPath.parse(content).read(jPath) : null;
    }

    static boolean isJPathInJson(String content, String jPath) {
        try {
            return JsonPath.parse(content).read(jPath) != null;
        } catch (Exception e) {
            return false;
        }
    }
}
