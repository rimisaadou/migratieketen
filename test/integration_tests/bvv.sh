# available variables:
# - $vreemdeling_id - id of newly created vreemdeling. Do not delete this vreemdeling, because other tests may depend on it.

echo 'Get specific vreemdeling from BVV:'
resp=$(curl $CURL_FLAGS \
    -H 'accept: application/json' \
    "http://np-bvv-backend-127.0.0.1.nip.io:8080/v0/vreemdelingen/$vreemdeling_id" \
)
printf '%s' "$resp" | jq .

echo 'Search vreemdelingen in BVV:'
resp=$(curl $CURL_FLAGS \
    -H 'accept: application/json' \
    'http://np-bvv-backend-127.0.0.1.nip.io:8080/v0/vreemdelingen?naam=john&geboortedatum=1978-12-31' \
)
printf '%s' "$resp" | jq .
