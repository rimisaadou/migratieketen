package application

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"regexp"
	"strings"
	"time"

	"github.com/google/uuid"
	openapi_types "github.com/oapi-codegen/runtime/types"
	api "gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/fictief-bvv-api"
	queries "gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/fictief-bvv-backend/pkg/storage/queries/generated"
	"golang.org/x/exp/maps"
)

// Implements api.StrictServerInterface.
func (app *Application) CreateSamenvoeging(ctx context.Context, request api.CreateSamenvoegingRequestObject) (api.CreateSamenvoegingResponseObject, error) {

	leidendId := request.VreemdelingLeidendId
	vervallenId := request.VreemdelingVervallenId
	createDate := time.Now()
	id := uuid.New()

	err := app.database.Queries.SamenvoegingInsert(ctx, &queries.SamenvoegingInsertParams{
		ID:                     id,
		VreemdelingLeidendID:   leidendId,
		VreemdelingVervallenID: vervallenId,
		CreatedAt:              createDate,
	})
	if err != nil {
		app.logger.Error("Error creating samenvoeging", "err", err)
		return nil, err
	}

	response := api.CreateSamenvoeging204JSONResponse{
		EmptyResponseJSONResponse: map[string]interface{}{},
	}

	return response, nil
}

// Implements api.StrictServerInterface.
func (app *Application) GetSamenvoegingen(ctx context.Context, request api.GetSamenvoegingenRequestObject) (api.GetSamenvoegingenResponseObject, error) {

	vreemdelingId := request.VreemdelingId

	vreemdelingRows, err := app.database.Queries.SamenvoegingGet(ctx, vreemdelingId)

	if err != nil {

		if !errors.Is(err, sql.ErrNoRows) {
			app.logger.Error("Error finding vreemdeling", "err", err)
			panic("Error finding vreemdeling")
		}
	}

	var vreemdelingen []api.VreemdelingId
	vreemdelingIds := make(map[api.VreemdelingId]struct{})
	for _, v := range vreemdelingRows {
		vreemdelingIds[v.VreemdelingLeidendID] = struct{}{}
		vreemdelingIds[v.VreemdelingVervallenID] = struct{}{}
	}
	vreemdelingen = maps.Keys(vreemdelingIds)

	response := api.GetSamenvoegingen200JSONResponse{

		VreemdelingIdsResponseJSONResponse: api.VreemdelingIdsResponseJSONResponse{
			Data: vreemdelingen,
		},
	}

	return response, nil
}

// Implements api.StrictServerInterface.
func (app *Application) CreateVreemdeling(ctx context.Context, request api.CreateVreemdelingRequestObject) (api.CreateVreemdelingResponseObject, error) {

	name := request.Body.Data.Naam
	searchname := ToSearchName(request.Body.Data.Naam)
	birthDate := request.Body.Data.Geboortedatum.Time
	id := uuid.New()

	count, err := app.database.Queries.VreemdelingCount(ctx)
	if err != nil {
		app.logger.Error("Error getting vreemdeling count", "err", err)
		return nil, err
	}

	vreemdelingNummer := fmt.Sprintf("%010d", count+1)

	err = app.database.Queries.VreemdelingCreate(ctx, &queries.VreemdelingCreateParams{
		ID:                id,
		Vreemdelingnummer: vreemdelingNummer,
		Name:              sql.NullString{String: name, Valid: true},
		Searchname:        sql.NullString{String: searchname, Valid: true},
		Birthdate:         sql.NullTime{Time: birthDate, Valid: true},
	})
	if err != nil {
		app.logger.Error("Error creating vreemdeling", "err", err)
		return nil, err
	}

	vreemdeling, err := app.findVreemdelingQuery(ctx, vreemdelingNummer)
	if err != nil {
		app.logger.Error("Error getting created vreemdeling", "err", err)
		return nil, err
	}

	response := api.CreateVreemdeling201JSONResponse{
		VreemdelingResponseJSONResponse: api.VreemdelingResponseJSONResponse{
			Data: *vreemdeling,
		},
	}

	return response, nil
}

// Implements api.StrictServerInterface.
func (app *Application) DeleteVreemdelingById(ctx context.Context, request api.DeleteVreemdelingByIdRequestObject) (api.DeleteVreemdelingByIdResponseObject, error) {
	vreemdelingNummer := request.VreemdelingId
	err := app.database.Queries.VreemdelingDeleteByNummer(ctx, vreemdelingNummer)
	if err != nil {
		app.logger.Error("Error removing vreemdeling", "err", err)
		return nil, err
	}

	response := api.DeleteVreemdelingById200JSONResponse{
		EmptyResponseJSONResponse: map[string]interface{}{},
	}

	return response, nil
}

// Implements api.StrictServerInterface.
func (app *Application) ReadVreemdelingById(ctx context.Context, request api.ReadVreemdelingByIdRequestObject) (api.ReadVreemdelingByIdResponseObject, error) {
	vreemdelingNummer := request.VreemdelingId

	vreemdeling, err := app.findVreemdelingQuery(ctx, vreemdelingNummer)
	if err != nil {
		app.logger.Error("Error finding vreemdeling", "err", err)

		if errors.Is(err, sql.ErrNoRows) {
			msg := err.Error()
			return api.ReadVreemdelingById410JSONResponse{
				GoneErrorResponseJSONResponse: api.GoneErrorResponseJSONResponse{
					Errors: []api.Error{
						{Message: &msg},
					},
				},
			}, nil
		}

		// Else (in case of another error)...
		return nil, err
	}

	response := api.ReadVreemdelingById200JSONResponse{
		VreemdelingResponseJSONResponse: api.VreemdelingResponseJSONResponse{
			Data: *vreemdeling,
		},
	}

	return response, nil
}

// Implements api.StrictServerInterface.
func (app *Application) UpdateVreemdelingById(ctx context.Context, request api.UpdateVreemdelingByIdRequestObject) (api.UpdateVreemdelingByIdResponseObject, error) {
	vreemdelingNummer := request.VreemdelingId
	var newVreemdelingNummer string
	if request.Body.Data.Id != "" {
		newVreemdelingNummer = request.Body.Data.Id
	} else {
		newVreemdelingNummer = vreemdelingNummer
	}

	params := queries.VreemdelingUpdateByNummerParams{
		Vreemdelingnummer:   vreemdelingNummer,
		Vreemdelingnummer_2: newVreemdelingNummer,
	}

	if request.Body.Data.Naam != "" {
		params.Name = sql.NullString{String: request.Body.Data.Naam, Valid: true}
		params.Searchname = sql.NullString{String: ToSearchName(request.Body.Data.Naam), Valid: true}
	}
	if request.Body.Data.Geboortedatum != nil {
		params.Birthdate = sql.NullTime{Time: request.Body.Data.Geboortedatum.Time, Valid: true}
	}

	err := app.database.Queries.VreemdelingUpdateByNummer(ctx, &params)
	if err != nil {
		app.logger.Error("Error updating vreemdeling", "err", err)
		return nil, err
	}

	vreemdeling, err := app.findVreemdelingQuery(ctx, newVreemdelingNummer)
	if err != nil {
		app.logger.Error("Error getting updated vreemdeling", "err", err)
		return nil, err
	}

	response := api.UpdateVreemdelingById200JSONResponse{
		VreemdelingResponseJSONResponse: api.VreemdelingResponseJSONResponse{
			Data: *vreemdeling,
		},
	}

	return response, nil
}

// FindVreemdeling implements api.StrictServerInterface.
func (app *Application) FindVreemdeling(ctx context.Context, request api.FindVreemdelingRequestObject) (api.FindVreemdelingResponseObject, error) {

	argName := sql.NullString{
		Valid: false,
	}

	if request.Params.Naam != nil {
		name := regexp.MustCompile("[_%]").ReplaceAllStringFunc(*request.Params.Naam, func(match string) string {
			return `\` + match
		})

		argName = sql.NullString{
			String: name,
			Valid:  *request.Params.Naam != "",
		}
	}

	argBirthdate := sql.NullTime{
		Valid: false,
	}

	if request.Params.Geboortedatum != nil {
		argBirthdate = sql.NullTime{
			Time:  request.Params.Geboortedatum.Time,
			Valid: true,
		}
	}

	args :=
		queries.VreemdelingSearchByNameAndBirthdateParams{
			Name:      argName,
			Birthdate: argBirthdate,
		}

	vreemdelingResult, err := app.database.Queries.VreemdelingSearchByNameAndBirthdate(ctx, &args)
	if err != nil {
		app.logger.Error("Error finding vreemdeling", "err", err)
		return nil, err
	}

	vreemdelingen := []api.Vreemdeling{}

	for _, dbVreemdeling := range vreemdelingResult {
		vreemdeling := convertVreemdelingFromDbToApi(*dbVreemdeling)
		vreemdelingen = append(vreemdelingen, vreemdeling)
	}

	response := api.FindVreemdeling200JSONResponse{
		VreemdelingenResponseJSONResponse: api.VreemdelingenResponseJSONResponse{
			Data: vreemdelingen,
		},
	}

	return response, nil
}

func ToSearchName(input string) string {
	return strings.ToLower(input)
}

func (app *Application) findVreemdelingQuery(ctx context.Context, vreemdelingNummer string) (*api.Vreemdeling, error) {
	vreemdelingResult, err := app.database.Queries.VreemdelingGetByNummer(ctx, vreemdelingNummer)
	if err != nil {
		return nil, err
	}

	vreemdeling := convertVreemdelingFromDbToApi(*vreemdelingResult)

	return &vreemdeling, nil
}

func convertVreemdelingFromDbToApi(dbVreemdeling queries.BvvBackendVreemdeling) api.Vreemdeling {
	birthDate := openapi_types.Date{
		Time: dbVreemdeling.Birthdate.Time,
	}

	return api.Vreemdeling{
		Geboortedatum: &birthDate,
		Id:            dbVreemdeling.Vreemdelingnummer,
		Naam:          dbVreemdeling.Name.String,
	}
}
