# Connect to an API for development purposes

If you want to connect to a migratieketen API to test it out, you can use a publicly exposed FSC Outway. The outway is
available on:

`https://migratieketen-fsc-outway.apps.digilab.network`

To protect this public endpoint we use Basic Authentication. The credentials will be provided during the Fieldlab.

To make a request to an API, you need to pass an FSC Grant Hash in the header of the request. FSC will use this to
direct the request to the proper API:

```bash
curl https://migratieketen-fsc-outway.apps.digilab.network \
  --user 'some-user:some-password' \
  --header 'Fsc-Grant-Hash: some-hash'
```

List of grant hashes per API:

| service                           | fsc grant hash                                                                                |
|-----------------------------------|-----------------------------------------------------------------------------------------------|
| fictief-coa-loket-backend         | `$1$3$pKq1AKtzTOnJrVCGrHXtdrVP6ekQSITCVxn4uhqF53MGD_FXlslm6jEdE0gTBaR43qgLh4Wb_j4EQne1iV9MTQ` |
| fictief-coa-sigma-backend         | `$1$3$oDS5_hswLFH4jDBx5vnybYvorpoN61I3VGsrTRRx9QqKPcG0GQdvUYp4NrXGSGM9YCbydyvxcw4HbV1P1Bk4Zg` |
| fictief-div-io-loket-backend      | `$1$3$EHy0HETa50cqr_5hhZxS_YaqH2z_U4jtKVBpLItE9zgHTR3RDk9lOd8FgfSx9i-sjnnHfFNmABsRxntMY1STVA` |
| fictief-div-io-sigma-backend      | `$1$3$JOxFZ1epO_WlMvCQW2NjV4ucQBLWxGmOQHZGsj0lKmG0Y7WR4gkxw6Sd7Zukgg88L84HnKonaoCbze6Y7mAC7g` |
| fictief-dji-loket-backend         | `$1$3$CGkgbu2WZh3wcc15XfYSYd9xKWV5Txu_IEAXpMpcwNwJ9V2qakp3XzMxE4Cxdt443tAZKo-xgb5HFkxdZM0hcQ` |
| fictief-dji-sigma-backend         | `$1$3$Ne8J8l9HpG18q7UbiO9-fpJHf1A4UEpuwzbAYkztBzq7jQ9iDR5rHIjGUwIlusJmeuK5UQPTpi3sMtrbffIzIg` |
| fictief-dtv-loket-backend         | `$1$3$I_uz18t3DqkwyTHBM6D0-JyhueQ2h3JlzAI_rSs_JSLbwahvKoh80MMw5Wa47IwQanctQKG_Sgy2k4emBtPopQ` |
| fictief-dtv-sigma-backend         | `$1$3$XQczZihZgsszbGheqb5_1I3FtsaNs6nFN3gE0j3edRhdvJE657Htcz9VtaU33n4egfLBjPlFHjXa2S-DChxQQA` |
| fictief-ind-loket-backend         | `$1$3$i6AnyeSOwMbn1viKNUzLMq5KJycvFkJb3Dta9R7mG5SF-UoL1Q0Y5PbjvVFErfOqolu4QLqWIBYkDgP9fiNSXw` |
| fictief-ind-sigma-backend         | `$1$3$5BvCiFyC_7qR4BgZ5viVrnmUrNfz59C1r5OW5leG6tsycG4POGW3AsA3PXui8Hpo5TVEpMrShgcj3v3lmbLrVQ` |
| fictief-np-bvv-backend            | `$1$3$4-EjSvx2DaxjHRJgW-n-KVyVbyBvASdzhTP6pc3V0A164YCc2TCxLtszbTF6WqbH0kbcAi95-JxjWYSntfZq_Q` |
| fictief-np-loket-backend          | `$1$3$uR2_I-S45VPmiZFgAQz62GhJKPLQMOilW58CJ15rVKizAcc74alGQdkl8bKiv7YIfJ6vaUOqGtEzsBGcJ_5RJg` |
| fictief-np-sigma-backend          | `$1$3$WuRW4k7FXa4cljZd-e_FTfkjLSoqSJ-swdpVrbc-Lb0Wyt2QM3MP74apai_jpSaDAiWw07DPjkAYkNkWUPhpGw` |
| fictief-rechtspraak-loket-backend | `$1$3$WfoMDgOqLeQXrFekj3AYZCM_bWO9LJFUMXAjgfA7IdUT-Wduy5sk16N6HKmOR4Rp8fPTFNGALWnO0MBX3cwMuQ` |
| fictief-rechtspraak-sigma-backend | `$1$3$HdeiZtJ4IjQrDFoLkWUkswjUHwQPbslwXgg7VIOud6zWzfnnuubzONjTJUXBVlE1HFtl_W6wWWnJ0h2BW6ItKg` |
