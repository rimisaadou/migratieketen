package main

import (
	"fmt"
	"log"
	"log/slog"
	"os"

	env "github.com/caarlos0/env/v10"
	"gopkg.in/yaml.v3"
)

type OrganizationConfig struct {
	ProcesNotifications []string `yaml:"procesNotifications"`
}

type Config struct {
	Organization  OrganizationConfig `yaml:"organization"`
	ServerAddress string             `env:"MK_ADDRESS" envDefault:":8081"`
	NatsAddress   string             `env:"MK_NATS_ADDRESS" envDefault:"ws://np-notifications-nats:8080"`
	ConfigPath    string             `env:"MK_CONFIG_PATH"`
}

func main() {
	log.SetFlags(0)

	logger := slog.New(slog.NewJSONHandler(os.Stdout, &slog.HandlerOptions{})).With("application", "notification_backend")
	logger.Info("starting up")

	if err := run(logger); err != nil {
		log.Fatal(err)
	}
}

func run(log *slog.Logger) error {
	proc := NewProcess()

	config, err := NewConfig()
	if err != nil {
		return fmt.Errorf("new config failed: %w", err)
	}

	notificationServer, err := NewNotificationServer(config, log)
	if err != nil {
		return err
	}

	if err := notificationServer.Run(); err != nil {
		return err
	}

	proc.Wait()

	return notificationServer.Shutdown()
}

func NewConfig() (*Config, error) {
	config := Config{}
	if err := env.Parse(&config); err != nil {
		return nil, fmt.Errorf("failed to parse env: %w", err)
	}

	file, err := os.ReadFile(config.ConfigPath)
	if err != nil {
		return nil, fmt.Errorf("failed to read organization config file: %w", err)
	}

	if err := yaml.Unmarshal(file, &config); err != nil {
		return nil, fmt.Errorf("failed to unmarshal config: %w", err)
	}

	return &config, nil
}
